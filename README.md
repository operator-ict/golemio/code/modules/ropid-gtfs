# @golemio/ropid-gtfs

:warning: **This module is deprecated**

> This module has been merged with the PID module. Please refer [here](https://gitlab.com/operator-ict/golemio/code/modules/pid).

Ropid GTFS module of the Golemio data platform

Developed by http://operatorict.cz

## TODO write this README
