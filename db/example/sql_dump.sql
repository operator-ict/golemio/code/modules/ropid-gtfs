-- EXAMPLE DATA - for testing purposes

INSERT INTO public.ropidgtfs_agency (agency_fare_url, agency_id, agency_lang, agency_name, agency_phone, agency_timezone, agency_url, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, agency_email) VALUES (NULL, '99', 'cs', 'Pražská integrovaná doprava', '+420234704560', 'Europe/Prague', 'https://pid.cz', NULL, '2019-05-27 03:20:15.469+00', NULL, NULL, '2019-05-27 03:20:15.469+00', NULL, NULL);

WITH init_values AS (SELECT NOW()::DATE AS now_date)
INSERT INTO public.ropidgtfs_calendar (end_date, friday, monday, saturday, service_id, start_date, sunday, thursday, tuesday, wednesday, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (TO_CHAR((SELECT now_date FROM init_values)+INTERVAL '7 days','YYYYMMDD'), 1, 1, 1, '1111111-1', TO_CHAR((SELECT now_date FROM init_values)-INTERVAL '1 days','YYYYMMDD'), 1, 1, 1, 1, NULL, '2019-05-27 03:20:15.632+00', NULL, NULL, '2019-05-27 03:20:15.632+00', NULL);

INSERT INTO public.ropidgtfs_routes (agency_id, is_night, is_regional, is_substitute_transport, route_color, route_desc, route_id, route_long_name, route_short_name, route_text_color, route_type, route_url, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES ('99', '0', '0', '0', '00A562', NULL, 'L991', 'Nemocnice Motol - Petřiny - Skalka - Depo Hostivař', 'A', 'FFFFFF', '1', 'https://pid.cz/linka/A', NULL, '2019-05-27 03:20:15.738+00', NULL, NULL, '2019-05-27 03:20:15.738+00', NULL);

INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0, 'L991V1', 50.0684100000000001, 14.5071700000000003, 1, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);
INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0.0171299999999999994, 'L991V1', 50.0684100000000001, 14.5069300000000005, 2, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);
INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0.132259999999999989, 'L991V1', 50.0684699999999978, 14.5053300000000007, 3, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);
INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0.233389999999999986, 'L991V1', 50.0685700000000011, 14.5039200000000008, 4, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);
INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0.349140000000000006, 'L991V1', 50.0687599999999975, 14.5023300000000006, 5, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);
INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0.538039999999999963, 'L991V2', 50.0691300000000012, 14.4997600000000002, 6, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);
INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0.597520000000000051, 'L991V3', 50.0693100000000015, 14.4989699999999999, 7, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);
INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0.651560000000000028, 'L991V4', 50.0695600000000027, 14.4983299999999993, 8, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);
INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0.732899999999999996, 'L991V5', 50.0699899999999971, 14.4974000000000007, 9, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);
INSERT INTO public.ropidgtfs_shapes (shape_dist_traveled, shape_id, shape_pt_lat, shape_pt_lon, shape_pt_sequence, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by) VALUES (0.853840000000000043, 'L991V2', 50.0706599999999966, 14.4960799999999992, 10, NULL, '2019-05-27 03:21:35.159+00', NULL, NULL, '2019-05-27 03:21:35.159+00', NULL);

INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:22:55', NULL, '7:22:55', NULL, '0', '0', 0, '', 'U953Z102P', 1, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:25:00', NULL, '7:25:30', NULL, '0', '0', 1.3758999999999999, '', 'U713Z102P', 2, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:27:00', NULL, '7:27:30', NULL, '1', '0', 2.60955000000000004, '', 'U921Z102P', 3, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:28:45', NULL, '7:29:05', NULL, '0', '1', 3.54618000000000011, '', 'U118Z102P', 4, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:30:15', NULL, '7:30:35', NULL, '0', '0', 4.40089000000000041, '', 'U209Z102P', 5, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:31:45', NULL, '7:32:05', NULL, '0', '0', 5.2504900000000001, '', 'U476Z102P', 6, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:33:15', NULL, '7:33:45', NULL, '0', '0', 6.10383000000000031, '', 'U400Z102P', 7, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:34:45', NULL, '7:35:15', NULL, '0', '0', 6.75666000000000011, '', 'U1072Z102P', 8, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:36:20', NULL, '7:36:40', NULL, '0', '0', 7.49417000000000044, '', 'U703Z102P', 9, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:37:45', NULL, '7:38:05', NULL, '0', '0', 8.27543000000000006, '', 'U360Z102P', 10, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:39:20', NULL, '7:39:50', NULL, '0', '0', 9.12764999999999915, '', 'U163Z102P', 11, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:41:10', NULL, '7:41:40', NULL, '0', '0', 9.95486999999999966, '', 'U321Z102P', 12, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:44:05', NULL, '7:44:25', NULL, '0', '0', 12.1394000000000002, '', 'U157Z102P', 13, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:45:50', NULL, '7:46:10', NULL, '0', '0', 13.1999700000000004, '', 'U462Z102P', 14, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:47:45', NULL, '7:48:15', NULL, '0', '0', 14.3343600000000002, '', 'U507Z102P', 15, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);
INSERT INTO public.ropidgtfs_stop_times (arrival_time, arrival_time_seconds, departure_time, departure_time_seconds, drop_off_type, pickup_type, shape_dist_traveled, stop_headsign, stop_id, stop_sequence, trip_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, timepoint) VALUES ('7:50:10', NULL, '7:50:10', NULL, '0', '0', 15.8698499999999996, '', 'U306Z102P', 16, '991_1151_190107', NULL, '2019-05-27 03:25:47.13+00', NULL, NULL, '2019-05-27 03:25:47.13+00', NULL, NULL);

INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U953S1', '2', 'U953Z102P', 50.0684400000000025, 14.5071700000000003, 'Skalka', '', 1, 'P', NULL, '2019-05-27 03:20:19.912+00', NULL, NULL, '2019-05-27 03:20:19.912+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U713S1', '2', 'U713Z102P', 50.0733400000000017, 14.4900900000000004, 'Strašnická', '', 1, 'P', NULL, '2019-05-27 03:20:18.808+00', NULL, NULL, '2019-05-27 03:20:18.808+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U921S1', '2', 'U921Z102P', 50.0785399999999967, 14.4748900000000003, 'Želivského', '', 2, 'P', NULL, '2019-05-27 03:20:18.808+00', NULL, NULL, '2019-05-27 03:20:18.808+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U118S1', '2', 'U118Z102P', 50.0782900000000026, 14.4618900000000004, 'Flora', '', 2, 'P', NULL, '2019-05-27 03:20:16.491+00', NULL, NULL, '2019-05-27 03:20:16.491+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U209S1', '2', 'U209Z102P', 50.0776400000000024, 14.4500399999999996, 'Jiřího z Poděbrad', '', 2, 'P', NULL, '2019-05-27 03:20:16.491+00', NULL, NULL, '2019-05-27 03:20:16.491+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U476S1', '2', 'U476Z102P', 50.0754000000000019, 14.4390800000000006, 'Náměstí Míru', '', 2, 'P', NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U376S1', '2', 'U276Z106P', 50.0754000000000019, 14.4390800000000006, 'I.P. Pavlova', '', 2, 'P', NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U576S1', '2', 'U376Z105P', 50.0754000000000019, 14.4390800000000006, 'Muzeum', '', 2, 'P', NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U776S1', '2', 'U476Z104P', 50.0754000000000019, 14.4390800000000006, 'Dejvická', '', 2, 'P', NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops (location_type, parent_station, platform_code, stop_id, stop_lat, stop_lon, stop_name, stop_url, wheelchair_boarding, zone_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, level_id, stop_code, stop_desc, stop_timezone) VALUES (0, 'U876S1', '2', 'U476Z103P', 50.0754000000000019, 14.4390800000000006, 'Nádraží Holešovice', '', 2, 'P', NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, '2019-05-27 03:20:17.945+00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops ("location_type", "parent_station", "platform_code", "stop_id", "stop_lat", "stop_lon", "stop_name", "stop_url", "wheelchair_boarding", "zone_id", "create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by", "level_id", "stop_code", "stop_desc", "stop_timezone") VALUES (0, 'U462S1', '2', 'U462Z102P', 50.09583, 14.34836, 'Nádraží Veleslavín', '', 1, 'P', -1, '2020-12-04 12:30:26.312286+01', 'integration-engine', NULL, NULL, NULL, 'U462L3', NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops ("location_type", "parent_station", "platform_code", "stop_id", "stop_lat", "stop_lon", "stop_name", "stop_url", "wheelchair_boarding", "zone_id", "create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by", "level_id", "stop_code", "stop_desc", "stop_timezone") VALUES (0, 'U306S1', '', 'U306Z102P', 50.07541, 14.34152, 'Nemocnice Motol', '', 1, 'P', -1, '2020-12-04 12:30:26.312286+01', 'integration-engine', NULL, NULL, NULL, 'U306L0', NULL, NULL, NULL);
INSERT INTO public.ropidgtfs_stops ("location_type", "parent_station", "platform_code", "stop_id", "stop_lat", "stop_lon", "stop_name", "stop_url", "wheelchair_boarding", "zone_id", "create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by", "level_id", "stop_code", "stop_desc", "stop_timezone") VALUES (1, '', '', 'U306S1', 50.07541, 14.34152, 'Nemocnice Motol', '', 1, 'P', -1, '2020-12-04 12:30:26.312286+01', 'integration-engine', NULL, NULL, NULL, '', NULL, NULL, NULL);

INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 0, 0, 'L991', '1111111-1', 'L991V1', 'Nemocnice Motol', '991_1151_190107', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');
INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 1, 0, 'L991', '1111111-1', 'L991V2', 'Depo Hostivař', '991_4_190107', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');
INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 0, 0, 'L991', '1111111-1', 'L991V3', 'Nemocnice Motol', '991_1152_190107', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');
INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 1, 0, 'L991', '1111111-1', 'L991V4', 'Depo Hostivař', '991_6_190107', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');
INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 0, 0, 'L991', '1111111-1', 'L991V5', 'Nemocnice Motol', '991_1153_190107', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');
INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 1, 0, 'L991', '1111111-1', 'L991V2', 'Depo Hostivař', '991_8_180709', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');
INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 0, 0, 'L991', '1111111-1', 'L991V3', 'Nemocnice Motol', '991_1154_180709', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');
INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 1, 0, 'L991', '1111111-1', 'L991V6', 'Skalka', '991_10_180709', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');
INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 0, 0, 'L991', '1111111-1', 'L991V1', 'Nemocnice Motol', '991_1155_180709', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');
INSERT INTO public.ropidgtfs_trips (bikes_allowed, block_id, direction_id, exceptional, route_id, service_id, shape_id, trip_headsign, trip_id, wheelchair_accessible, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by, trip_operation_type, trip_short_name) VALUES (1, '', 1, 0, 'L991', '1111111-1', 'L991V2', 'Depo Hostivař', '991_12_180709', 1, NULL, '2019-05-27 03:20:24.44+00', NULL, NULL, '2019-05-27 03:20:24.44+00', NULL, 1, '');

WITH init_values AS (SELECT NOW()::DATE AS now_date)
INSERT INTO public.ropidgtfs_calendar_dates (date, exception_type, service_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES (TO_CHAR((SELECT now_date FROM init_values)+INTERVAL '2 days','YYYY-MM-DD'),	2,	'1111111-1',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(TO_CHAR((SELECT now_date FROM init_values)+INTERVAL '14 days','YYYY-MM-DD'),	1,	'1111111-1',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);


-- >>> FOR STOPS ASW & CIS PARAMS TESTS

INSERT INTO "ropidgtfs_stops" ("location_type", "parent_station", "platform_code", "stop_id", "stop_lat", "stop_lon", "stop_name", "stop_url", "wheelchair_boarding", "zone_id", "create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by", "level_id", "stop_code", "stop_desc", "stop_timezone") VALUES
(0,	'',	'A',	'U286Z1P',	50.03046,	14.52744,	'Háje',	'',	0,	'P',	-1,	'2020-11-19 12:30:36.671771+01',	'integration-engine',	NULL,	NULL,	NULL,	'',	NULL,	NULL,	NULL),
(0,	'',	'B',	'U286Z2P',	50.03045,	14.527,	'Háje',	'',	0,	'P',	-1,	'2020-11-19 12:30:36.671771+01',	'integration-engine',	NULL,	NULL,	NULL,	'',	NULL,	NULL,	NULL),
(0,	'',	'C',	'U286Z3P',	50.03038,	14.52585,	'Háje',	'',	0,	'P',	-1,	'2020-11-19 12:30:36.671771+01',	'integration-engine',	NULL,	NULL,	NULL,	'',	NULL,	NULL,	NULL),
(0,	'',	'D',	'U286Z4',	50.03036,	14.52538,	'Háje',	'',	0,	'0',	-1,	'2020-11-19 12:30:36.671771+01',	'integration-engine',	NULL,	NULL,	NULL,	'',	NULL,	NULL,	NULL),
(0,	'',	'E',	'U286Z5P',	50.03028,	14.52752,	'Háje',	'',	0,	'P',	-1,	'2020-11-19 12:30:36.671771+01',	'integration-engine',	NULL,	NULL,	NULL,	'',	NULL,	NULL,	NULL),
(0,	'',	'C',	'U286Z13P',	50.03038,	14.52585,	'Háje',	'',	0,	'P',	-1,	'2020-11-19 12:30:36.671771+01',	'integration-engine',	NULL,	NULL,	NULL,	'',	NULL,	NULL,	NULL),
(0,	'',	'B',	'U286Z72',	50.03045,	14.527,	'Háje',	'',	0,	'0',	-1,	'2020-11-19 12:30:36.671771+01',	'integration-engine',	NULL,	NULL,	NULL,	'',	NULL,	NULL,	NULL),
(0,	'U286S1',	'1',	'U286Z101P',	50.03083,	14.52696,	'Háje',	'',	1,	'P',	-1,	'2020-11-19 12:30:36.671771+01',	'integration-engine',	NULL,	NULL,	NULL,	'U286L2',	NULL,	NULL,	NULL),
(0,	'U286S1',	'2',	'U286Z102P',	50.03074,	14.52698,	'Háje',	'',	1,	'P',	-1,	'2020-11-19 12:30:36.671771+01',	'integration-engine',	NULL,	NULL,	NULL,	'U286L2',	NULL,	NULL,	NULL);

INSERT INTO "ropidgtfs_cis_stops" ("alt_idos_name", "cis", "id", "jtsk_x", "jtsk_y", "lat", "lon", "platform", "created_at", "wheelchair_access", "zone", "create_batch_id", "created_by", "update_batch_id", "updated_at", "updated_by") VALUES
('Háje (ul. Opatovská)',	55083,	'286/1',	-736136.5,	-1050325.75,	50.03046,	14.527442,	'A',	'2020-11-20 12:30:11.942+01',	'unknown',	'P',	NULL,	NULL,	NULL,	'2020-11-20 12:30:11.942+01',	NULL),
('Háje',	55083,	'286/2',	-736168.4,	-1050322.63,	50.03045,	14.5269957,	'B',	'2020-11-20 12:30:11.942+01',	'unknown',	'P,0',	NULL,	NULL,	NULL,	'2020-11-20 12:30:11.942+01',	NULL),
('Háje',	55083,	'286/3',	-736250.9,	-1050318.63,	50.0303841,	14.5258474,	'C',	'2020-11-20 12:30:11.942+01',	'unknown',	'P',	NULL,	NULL,	NULL,	'2020-11-20 12:30:11.942+01',	NULL),
('Háje',	55083,	'286/4',	-736284.5,	-1050316.5,	50.03036,	14.5253782,	'D',	'2020-11-20 12:30:11.942+01',	'unknown',	'P,0',	NULL,	NULL,	NULL,	'2020-11-20 12:30:11.942+01',	NULL),
('Háje (ul. Opatovská)',	55083,	'286/5',	-736133.438,	-1050346.13,	50.03028,	14.527523,	'E',	'2020-11-20 12:30:11.942+01',	'unknown',	'P,0',	NULL,	NULL,	NULL,	'2020-11-20 12:30:11.942+01',	NULL),
('Háje',	55083,	'286/13',	-736250.9,	-1050318.63,	50.0303841,	14.5258474,	'C',	'2020-11-20 12:30:11.942+01',	'unknown',	'P',	NULL,	NULL,	NULL,	'2020-11-20 12:30:11.942+01',	NULL),
('Háje',	55083,	'286/72',	-736168.4,	-1050322.63,	50.03045,	14.5269957,	'B',	'2020-11-20 12:30:11.942+01',	'unknown',	'0',	NULL,	NULL,	NULL,	'2020-11-20 12:30:11.942+01',	NULL),
('Háje',	55083,	'286/101',	-736215.063,	-1050281.63,	50.0307579,	14.5262728,	'M1',	'2020-11-20 12:30:11.942+01',	'possible',	'P',	NULL,	NULL,	NULL,	'2020-11-20 12:30:11.942+01',	NULL),
('Háje',	55083,	'286/102',	-736114.75,	-1050289.25,	50.03081,	14.5276747,	'M2',	'2020-11-20 12:30:11.942+01',	'possible',	'P',	NULL,	NULL,	NULL,	'2020-11-20 12:30:11.942+01',	NULL);

-- <<<

-- >>> SPECIAL CASE WITH RENAMING STOPS
-- first origin, second with suffix _DATE in stop_id with changed attribute, i.e. stop_name

INSERT INTO "ropidgtfs_cis_stops" ("alt_idos_name", "cis", "id", "jtsk_x", "jtsk_y", "lat", "lon", "platform", "created_at", "wheelchair_access", "zone", "create_batch_id", "created_by", "update_batch_id", "updated_at", "updated_by") VALUES
('Fučíkova',	27878,	'115/101',	-741166.3,	-1040788.06,	50.1092949,	14.4397736,	'M1',	'2020-11-26 12:30:18.675+01',	'possible',	'P',	NULL,	NULL,	NULL,	'2020-11-26 12:30:18.675+01',	NULL),
('Fučíkova',	27878,	'115/102',	-741135.438,	-1040877.69,	50.1085358,	14.4403725,	'M2',	'2020-11-26 12:30:18.675+01',	'possible',	'P',	NULL,	NULL,	NULL,	'2020-11-26 12:30:18.675+01',	NULL);

INSERT INTO "ropidgtfs_stops" ("location_type", "parent_station", "platform_code", "stop_id", "stop_lat", "stop_lon", "stop_name", "stop_url", "wheelchair_boarding", "zone_id", "create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by", "level_id", "stop_code", "stop_desc", "stop_timezone") VALUES
(0,	'U115S1',	'1',	'U115Z101P',	50.10896,	14.44016,	'Fučíkova',	'',	1,	'P',	-1,	'2020-11-24 12:30:42.560064+01',	'integration-engine',	NULL,	NULL,	NULL,	'U115L1',	NULL,	NULL,	NULL),
(0,	'U115S1',	'2',	'U115Z102P',	50.10892,	14.44002,	'Fučíkova',	'',	1,	'P',	-1,	'2020-11-24 12:30:42.560064+01',	'integration-engine',	NULL,	NULL,	NULL,	'U115L1',	NULL,	NULL,	NULL),
(0,	'U115S1',	'1',	'U115Z101P_900222',	50.10896,	14.44016,	'Nádraží Holešovice',	'',	1,	'P',	-1,	'2020-11-24 12:30:42.560064+01',	'integration-engine',	NULL,	NULL,	NULL,	'U115L1',	NULL,	NULL,	NULL),
(0,	'U115S1',	'2',	'U115Z102P_900222',	50.10892,	14.44002,	'Nádraží Holešovice',	'',	1,	'P',	-1,	'2020-11-24 12:30:42.560064+01',	'integration-engine',	NULL,	NULL,	NULL,	'U115L1',	NULL,	NULL,	NULL);

-- <<<



-- >>> Two routes with few common stops

INSERT INTO "public"."ropidgtfs_routes" ("agency_id","is_night","route_color","route_desc","route_id","route_long_name","route_short_name","route_text_color","route_type","route_url","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","is_regional","is_substitute_transport")
VALUES
(E'99',E'0',E'007DA8',NULL,E'L152',E'Českomoravská - Sídliště Čimice',E'152',E'FFFFFF',E'3',E'https://pid.cz/linka/152',-1,E'2021-04-14 05:40:41.212+02',NULL,-1,E'2021-04-14 05:40:41.212+02',NULL,E'0',E'0'),
(E'99',E'0',E'007DA8',NULL,E'L177',E'Chodov - Skalka - Poliklinika Mazurská',E'177',E'FFFFFF',E'3',E'https://pid.cz/linka/177',-1,E'2021-04-14 05:40:41.217+02',NULL,-1,E'2021-04-14 05:40:41.217+02',NULL,E'0',E'0');


INSERT INTO "public"."ropidgtfs_trips"("bikes_allowed","block_id","direction_id","exceptional","route_id","service_id","shape_id","trip_headsign","trip_id","wheelchair_accessible","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","trip_operation_type","trip_short_name")
VALUES
(2,NULL,1,0,E'L152',E'1111111-1',E'L152V1',E'Sídliště Čimice',E'152_37_201230',1,-1,E'2021-04-14 05:40:44.535+02',NULL,-1,E'2021-04-14 05:40:44.535+02',NULL,1,NULL),
(2,NULL,1,0,E'L152',E'1111111-1',E'L152VX',E'Kobylisy',E'152_60_201230',1,-1,E'2021-04-14 05:40:44.535+02',NULL,-1,E'2021-04-14 05:40:44.535+02',NULL,1,NULL),
(2,NULL,1,0,E'L152',E'1111111-1',E'L152V1',E'Sídliště Čimice',E'152_61_201230',1,-1,E'2021-04-14 05:40:44.535+02',NULL,-1,E'2021-04-14 05:40:44.535+02',NULL,1,NULL),
(2,NULL,1,0,E'L177',E'1111111-1',E'L177V3',E'Poliklinika Mazurská',E'177_110_210201',1,-1,E'2021-04-14 05:40:44.598+02',NULL,-1,E'2021-04-14 05:40:44.598+02',NULL,1,NULL),
(2,NULL,1,0,E'L177',E'1111111-1',E'L177V3',E'Poliklinika Mazurská',E'177_121_210201',1,-1,E'2021-04-14 05:40:44.599+02',NULL,-1,E'2021-04-14 05:40:44.599+02',NULL,1,NULL);

INSERT INTO "public"."ropidgtfs_stop_times"("arrival_time","arrival_time_seconds","departure_time","departure_time_seconds","drop_off_type","pickup_type","shape_dist_traveled","stop_headsign","stop_id","stop_sequence","trip_id","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","timepoint")
VALUES
(E'11:34:00',NULL,E'11:34:00',NULL,E'0',E'0',0,NULL,E'U510Z2P',1,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:37:00',NULL,E'11:37:00',NULL,E'0',E'0',1.41799,NULL,E'U474Z4P',2,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:42:00',NULL,E'11:42:00',NULL,E'0',E'0',3.36004,NULL,E'U603Z3P',3,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:43:00',NULL,E'11:43:00',NULL,E'0',E'0',3.82975,NULL,E'U665Z2P',4,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:44:00',NULL,E'11:44:00',NULL,E'0',E'0',4.25536,NULL,E'U332Z2P',5,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:46:00',NULL,E'11:46:00',NULL,E'0',E'0',4.89398,NULL,E'U467Z2P',6,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:48:00',NULL,E'11:48:00',NULL,E'3',E'3',5.39679,NULL,E'U740Z3P',7,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:50:00',NULL,E'11:50:00',NULL,E'0',E'0',5.83914,NULL,E'U78Z3P',8,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:53:00',NULL,E'11:53:00',NULL,E'0',E'0',6.93061,NULL,E'U675Z3P',9,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:54:00',NULL,E'11:54:00',NULL,E'3',E'3',7.41981,NULL,E'U267Z1P',10,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:55:00',NULL,E'11:55:00',NULL,E'3',E'3',7.89958,NULL,E'U543Z1P',11,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:56:00',NULL,E'11:56:00',NULL,E'3',E'3',8.46999,NULL,E'U544Z1P',12,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:58:00',NULL,E'11:58:00',NULL,E'0',E'0',9.02079,NULL,E'U581Z1P',13,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'11:59:00',NULL,E'11:59:00',NULL,E'0',E'0',9.44131,NULL,E'U806Z1P',14,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'12:00:00',NULL,E'12:00:00',NULL,E'3',E'3',9.74564,NULL,E'U330Z1P',15,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'12:01:00',NULL,E'12:01:00',NULL,E'0',E'0',10.20839,NULL,E'U74Z1P',16,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'12:02:00',NULL,E'12:02:00',NULL,E'0',E'0',10.49467,NULL,E'U949Z1P',17,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),
(E'12:04:00',NULL,E'12:04:00',NULL,E'0',E'0',10.73493,NULL,E'U949Z2P',18,E'152_37_201230',-1,E'2021-04-14 05:41:11.173+02',NULL,-1,E'2021-04-14 05:41:11.173+02',NULL,NULL),

(E'11:39:00',NULL,E'11:39:00',NULL,E'0',E'0',0,NULL,E'U510Z2P',1,E'152_60_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:42:00',NULL,E'11:42:00',NULL,E'0',E'0',1.41799,NULL,E'U474Z4P',2,E'152_60_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:47:00',NULL,E'11:47:00',NULL,E'0',E'0',3.36004,NULL,E'U603Z3P',3,E'152_60_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:48:00',NULL,E'11:48:00',NULL,E'0',E'0',3.82975,NULL,E'U665Z2P',4,E'152_60_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:49:00',NULL,E'11:49:00',NULL,E'0',E'0',4.25536,NULL,E'U332Z2P',5,E'152_60_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:51:00',NULL,E'11:51:00',NULL,E'0',E'0',4.89398,NULL,E'U467Z2P',6,E'152_60_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:53:00',NULL,E'11:53:00',NULL,E'3',E'3',5.39679,NULL,E'U740Z3P',7,E'152_60_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:55:00',NULL,E'11:55:00',NULL,E'0',E'0',5.83914,NULL,E'U78Z3P',8,E'152_60_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:58:00',NULL,E'11:58:00',NULL,E'0',E'0',6.93061,NULL,E'U675Z3P',9,E'152_60_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),

(E'11:49:00',NULL,E'11:49:00',NULL,E'0',E'0',0,NULL,E'U510Z2P',1,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:52:00',NULL,E'11:52:00',NULL,E'0',E'0',1.41799,NULL,E'U474Z4P',2,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:57:00',NULL,E'11:57:00',NULL,E'0',E'0',3.36004,NULL,E'U603Z3P',3,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:58:00',NULL,E'11:58:00',NULL,E'0',E'0',3.82975,NULL,E'U665Z2P',4,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'11:59:00',NULL,E'11:59:00',NULL,E'0',E'0',4.25536,NULL,E'U332Z2P',5,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:01:00',NULL,E'12:01:00',NULL,E'0',E'0',4.89398,NULL,E'U467Z2P',6,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:03:00',NULL,E'12:03:00',NULL,E'3',E'3',5.39679,NULL,E'U740Z3P',7,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:05:00',NULL,E'12:05:00',NULL,E'0',E'0',5.83914,NULL,E'U78Z3P',8,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:08:00',NULL,E'12:08:00',NULL,E'0',E'0',6.93061,NULL,E'U675Z3P',9,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:09:00',NULL,E'12:09:00',NULL,E'3',E'3',7.41981,NULL,E'U267Z1P',10,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:10:00',NULL,E'12:10:00',NULL,E'3',E'3',7.89958,NULL,E'U543Z1P',11,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:11:00',NULL,E'12:11:00',NULL,E'3',E'3',8.46999,NULL,E'U544Z1P',12,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:13:00',NULL,E'12:13:00',NULL,E'0',E'0',9.02079,NULL,E'U581Z1P',13,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:14:00',NULL,E'12:14:00',NULL,E'0',E'0',9.44131,NULL,E'U806Z1P',14,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:15:00',NULL,E'12:15:00',NULL,E'3',E'3',9.74564,NULL,E'U330Z1P',15,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:16:00',NULL,E'12:16:00',NULL,E'0',E'0',10.20839,NULL,E'U74Z1P',16,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:17:00',NULL,E'12:17:00',NULL,E'0',E'0',10.49467,NULL,E'U949Z1P',17,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),
(E'12:19:00',NULL,E'12:19:00',NULL,E'0',E'0',10.73493,NULL,E'U949Z2P',18,E'152_61_201230',-1,E'2021-04-14 05:41:11.186+02',NULL,-1,E'2021-04-14 05:41:11.186+02',NULL,NULL),

(E'10:39:00',NULL,E'10:39:00',NULL,E'0',E'0',0,NULL,E'U52Z5P',1,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:41:00',NULL,E'10:41:00',NULL,E'0',E'0',0.685,NULL,E'U1116Z3P',2,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:43:00',NULL,E'10:43:00',NULL,E'3',E'3',1.61632,NULL,E'U972Z1P',3,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:44:00',NULL,E'10:44:00',NULL,E'0',E'0',1.96813,NULL,E'U982Z1P',4,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:46:00',NULL,E'10:46:00',NULL,E'3',E'3',3.01437,NULL,E'U774Z1P',5,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:48:00',NULL,E'10:48:00',NULL,E'0',E'0',3.96711,NULL,E'U106Z6P',6,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:49:00',NULL,E'10:49:00',NULL,E'0',E'0',4.47048,NULL,E'U344Z3P',7,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:51:00',NULL,E'10:51:00',NULL,E'0',E'0',4.93033,NULL,E'U99Z2P',8,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:54:00',NULL,E'10:54:00',NULL,E'3',E'3',5.98476,NULL,E'U195Z2P',9,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:55:00',NULL,E'10:55:00',NULL,E'3',E'3',6.37158,NULL,E'U409Z2P',10,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:56:00',NULL,E'10:56:00',NULL,E'0',E'0',6.67323,NULL,E'U166Z2P',11,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:57:00',NULL,E'10:57:00',NULL,E'3',E'3',7.14958,NULL,E'U641Z2P',12,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'10:59:00',NULL,E'10:59:00',NULL,E'3',E'3',8.06919,NULL,E'U160Z2P',13,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:01:00',NULL,E'11:01:00',NULL,E'0',E'0',8.77848,NULL,E'U596Z3P',14,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:02:00',NULL,E'11:02:00',NULL,E'0',E'0',9.37708,NULL,E'U670Z4P',15,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:04:00',NULL,E'11:04:00',NULL,E'0',E'0',10.07401,NULL,E'U889Z1P',16,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:06:00',NULL,E'11:06:00',NULL,E'0',E'0',10.58733,NULL,E'U673Z4P',17,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:08:00',NULL,E'11:08:00',NULL,E'0',E'0',11.02709,NULL,E'U953Z7P',18,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:09:00',NULL,E'11:09:00',NULL,E'3',E'3',11.53931,NULL,E'U1222Z1P',19,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:11:00',NULL,E'11:11:00',NULL,E'0',E'0',12.21761,NULL,E'U827Z2P',20,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:13:00',NULL,E'11:13:00',NULL,E'0',E'0',12.85245,NULL,E'U660Z5P',21,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:15:00',NULL,E'11:15:00',NULL,E'3',E'3',13.24008,NULL,E'U359Z3P',22,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:18:00',NULL,E'11:18:00',NULL,E'0',E'0',14.26823,NULL,E'U574Z2P',23,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:20:00',NULL,E'11:20:00',NULL,E'0',E'0',15.395,NULL,E'U694Z4P',24,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:21:00',NULL,E'11:21:00',NULL,E'3',E'3',15.76779,NULL,E'U13Z1P',25,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:23:00',NULL,E'11:23:00',NULL,E'3',E'3',16.40167,NULL,E'U225Z2P',26,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:26:00',NULL,E'11:26:00',NULL,E'0',E'0',17.11724,NULL,E'U134Z5P',27,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:28:00',NULL,E'11:28:00',NULL,E'0',E'0',17.99732,NULL,E'U474Z4P',28,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:33:00',NULL,E'11:33:00',NULL,E'0',E'0',19.93937,NULL,E'U603Z3P',29,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:34:00',NULL,E'11:34:00',NULL,E'0',E'0',20.40908,NULL,E'U665Z2P',30,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:35:00',NULL,E'11:35:00',NULL,E'0',E'0',20.83469,NULL,E'U332Z2P',31,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:37:00',NULL,E'11:37:00',NULL,E'0',E'0',21.47331,NULL,E'U467Z2P',32,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:39:00',NULL,E'11:39:00',NULL,E'3',E'3',21.97611,NULL,E'U740Z3P',33,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:41:00',NULL,E'11:41:00',NULL,E'0',E'0',22.41847,NULL,E'U78Z3P',34,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:44:00',NULL,E'11:44:00',NULL,E'0',E'0',23.50994,NULL,E'U675Z3P',35,E'177_110_210201',-1,E'2021-04-14 05:41:12.737+02',NULL,-1,E'2021-04-14 05:41:12.737+02',NULL,NULL),
(E'11:45:00',NULL,E'11:45:00',NULL,E'3',E'3',23.99914,NULL,E'U267Z1P',36,E'177_110_210201',-1,E'2021-04-14 05:41:12.738+02',NULL,-1,E'2021-04-14 05:41:12.738+02',NULL,NULL),
(E'11:46:00',NULL,E'11:46:00',NULL,E'3',E'3',24.47891,NULL,E'U543Z1P',37,E'177_110_210201',-1,E'2021-04-14 05:41:12.738+02',NULL,-1,E'2021-04-14 05:41:12.738+02',NULL,NULL),
(E'11:47:00',NULL,E'11:47:00',NULL,E'3',E'3',25.04932,NULL,E'U544Z1P',38,E'177_110_210201',-1,E'2021-04-14 05:41:12.738+02',NULL,-1,E'2021-04-14 05:41:12.738+02',NULL,NULL),
(E'11:49:00',NULL,E'11:49:00',NULL,E'0',E'0',25.60012,NULL,E'U581Z1P',39,E'177_110_210201',-1,E'2021-04-14 05:41:12.738+02',NULL,-1,E'2021-04-14 05:41:12.738+02',NULL,NULL),
(E'11:50:00',NULL,E'11:50:00',NULL,E'0',E'0',26.07637,NULL,E'U345Z1P',40,E'177_110_210201',-1,E'2021-04-14 05:41:12.738+02',NULL,-1,E'2021-04-14 05:41:12.738+02',NULL,NULL),
(E'11:51:00',NULL,E'11:51:00',NULL,E'0',E'0',26.5109,NULL,E'U511Z1P',41,E'177_110_210201',-1,E'2021-04-14 05:41:12.738+02',NULL,-1,E'2021-04-14 05:41:12.738+02',NULL,NULL),
(E'11:52:00',NULL,E'11:52:00',NULL,E'0',E'0',26.99361,NULL,E'U912Z4P',42,E'177_110_210201',-1,E'2021-04-14 05:41:12.738+02',NULL,-1,E'2021-04-14 05:41:12.738+02',NULL,NULL),
(E'11:53:00',NULL,E'11:53:00',NULL,E'0',E'0',27.32129,NULL,E'U371Z1P',43,E'177_110_210201',-1,E'2021-04-14 05:41:12.738+02',NULL,-1,E'2021-04-14 05:41:12.738+02',NULL,NULL),
(E'11:55:00',NULL,E'11:55:00',NULL,E'0',E'0',27.63007,NULL,E'U649Z1P',44,E'177_110_210201',-1,E'2021-04-14 05:41:12.738+02',NULL,-1,E'2021-04-14 05:41:12.738+02',NULL,NULL),

(E'10:54:00',NULL,E'10:54:00',NULL,E'0',E'0',0,NULL,E'U52Z5P',1,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'10:56:00',NULL,E'10:56:00',NULL,E'0',E'0',0.685,NULL,E'U1116Z3P',2,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'10:58:00',NULL,E'10:58:00',NULL,E'3',E'3',1.61632,NULL,E'U972Z1P',3,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'10:59:00',NULL,E'10:59:00',NULL,E'0',E'0',1.96813,NULL,E'U982Z1P',4,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:01:00',NULL,E'11:01:00',NULL,E'3',E'3',3.01437,NULL,E'U774Z1P',5,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:03:00',NULL,E'11:03:00',NULL,E'0',E'0',3.96711,NULL,E'U106Z6P',6,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:04:00',NULL,E'11:04:00',NULL,E'0',E'0',4.47048,NULL,E'U344Z3P',7,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:06:00',NULL,E'11:06:00',NULL,E'0',E'0',4.93033,NULL,E'U99Z2P',8,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:09:00',NULL,E'11:09:00',NULL,E'3',E'3',5.98476,NULL,E'U195Z2P',9,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:10:00',NULL,E'11:10:00',NULL,E'3',E'3',6.37158,NULL,E'U409Z2P',10,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:11:00',NULL,E'11:11:00',NULL,E'0',E'0',6.67323,NULL,E'U166Z2P',11,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:12:00',NULL,E'11:12:00',NULL,E'3',E'3',7.14958,NULL,E'U641Z2P',12,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:14:00',NULL,E'11:14:00',NULL,E'3',E'3',8.06919,NULL,E'U160Z2P',13,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:16:00',NULL,E'11:16:00',NULL,E'0',E'0',8.77848,NULL,E'U596Z3P',14,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:17:00',NULL,E'11:17:00',NULL,E'0',E'0',9.37708,NULL,E'U670Z4P',15,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:19:00',NULL,E'11:19:00',NULL,E'0',E'0',10.07401,NULL,E'U889Z1P',16,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:21:00',NULL,E'11:21:00',NULL,E'0',E'0',10.58733,NULL,E'U673Z4P',17,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:23:00',NULL,E'11:23:00',NULL,E'0',E'0',11.02709,NULL,E'U953Z7P',18,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:24:00',NULL,E'11:24:00',NULL,E'3',E'3',11.53931,NULL,E'U1222Z1P',19,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:26:00',NULL,E'11:26:00',NULL,E'0',E'0',12.21761,NULL,E'U827Z2P',20,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:28:00',NULL,E'11:28:00',NULL,E'0',E'0',12.85245,NULL,E'U660Z5P',21,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:30:00',NULL,E'11:30:00',NULL,E'3',E'3',13.24008,NULL,E'U359Z3P',22,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:33:00',NULL,E'11:33:00',NULL,E'0',E'0',14.26823,NULL,E'U574Z2P',23,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:35:00',NULL,E'11:35:00',NULL,E'0',E'0',15.395,NULL,E'U694Z4P',24,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:36:00',NULL,E'11:36:00',NULL,E'3',E'3',15.76779,NULL,E'U13Z1P',25,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:38:00',NULL,E'11:38:00',NULL,E'3',E'3',16.40167,NULL,E'U225Z2P',26,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:41:00',NULL,E'11:41:00',NULL,E'0',E'0',17.11724,NULL,E'U134Z5P',27,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:43:00',NULL,E'11:43:00',NULL,E'0',E'0',17.99732,NULL,E'U474Z4P',28,E'177_121_210201',-1,E'2021-04-14 05:41:12.75+02',NULL,-1,E'2021-04-14 05:41:12.75+02',NULL,NULL),
(E'11:48:00',NULL,E'11:48:00',NULL,E'0',E'0',19.93937,NULL,E'U603Z3P',29,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'11:49:00',NULL,E'11:49:00',NULL,E'0',E'0',20.40908,NULL,E'U665Z2P',30,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'11:50:00',NULL,E'11:50:00',NULL,E'0',E'0',20.83469,NULL,E'U332Z2P',31,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'11:52:00',NULL,E'11:52:00',NULL,E'0',E'0',21.47331,NULL,E'U467Z2P',32,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'11:54:00',NULL,E'11:54:00',NULL,E'3',E'3',21.97611,NULL,E'U740Z3P',33,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'11:56:00',NULL,E'11:56:00',NULL,E'0',E'0',22.41847,NULL,E'U78Z3P',34,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'11:59:00',NULL,E'11:59:00',NULL,E'0',E'0',23.50994,NULL,E'U675Z3P',35,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'12:00:00',NULL,E'12:00:00',NULL,E'3',E'3',23.99914,NULL,E'U267Z1P',36,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'12:01:00',NULL,E'12:01:00',NULL,E'3',E'3',24.47891,NULL,E'U543Z1P',37,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'12:02:00',NULL,E'12:02:00',NULL,E'3',E'3',25.04932,NULL,E'U544Z1P',38,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'12:04:00',NULL,E'12:04:00',NULL,E'0',E'0',25.60012,NULL,E'U581Z1P',39,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'12:05:00',NULL,E'12:05:00',NULL,E'0',E'0',26.07637,NULL,E'U345Z1P',40,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'12:06:00',NULL,E'12:06:00',NULL,E'0',E'0',26.5109,NULL,E'U511Z1P',41,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'12:07:00',NULL,E'12:07:00',NULL,E'0',E'0',26.99361,NULL,E'U912Z4P',42,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'12:08:00',NULL,E'12:08:00',NULL,E'0',E'0',27.32129,NULL,E'U371Z1P',43,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL),
(E'12:10:00',NULL,E'12:10:00',NULL,E'0',E'0',27.63007,NULL,E'U649Z1P',44,E'177_121_210201',-1,E'2021-04-14 05:41:12.751+02',NULL,-1,E'2021-04-14 05:41:12.751+02',NULL,NULL);


INSERT INTO "public"."ropidgtfs_stops"("location_type","parent_station","platform_code","stop_id","stop_lat","stop_lon","stop_name","stop_url","wheelchair_boarding","zone_id","create_batch_id","created_at","created_by","update_batch_id","updated_at","updated_by","level_id","stop_code","stop_desc","stop_timezone")
VALUES
(0,NULL,E'F',E'U106Z6P',50.02823,14.50805,E'Opatov',NULL,0,E'P',-1,E'2021-04-14 05:40:41.542+02',NULL,-1,E'2021-04-14 05:40:41.542+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'C',E'U1116Z3P',50.03073,14.48755,E'Petýrkova',NULL,0,E'P',-1,E'2021-04-14 05:40:41.733+02',NULL,-1,E'2021-04-14 05:40:41.733+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U1222Z1P',50.07265,14.50826,E'Donatellova',NULL,0,E'P',-1,E'2021-04-14 05:40:41.741+02',NULL,-1,E'2021-04-14 05:40:41.741+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'E',E'U134Z5P',50.10285,14.5032,E'Nádraží Libeň',NULL,0,E'P',-1,E'2021-04-14 05:40:41.549+02',NULL,-1,E'2021-04-14 05:40:41.549+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U13Z1P',50.0954,14.49626,E'Balkán',NULL,0,E'P',-1,E'2021-04-14 05:40:41.412+02',NULL,-1,E'2021-04-14 05:40:41.412+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U160Z2P',50.05013,14.52643,E'Hostivařské náměstí',NULL,0,E'P',-1,E'2021-04-14 05:40:41.555+02',NULL,-1,E'2021-04-14 05:40:41.555+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U166Z2P',50.04775,14.5196,E'Toulcův dvůr',NULL,0,E'P',-1,E'2021-04-14 05:40:41.556+02',NULL,-1,E'2021-04-14 05:40:41.556+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U195Z2P',50.04479,14.51247,E'Přeštická',NULL,0,E'P',-1,E'2021-04-14 05:40:41.564+02',NULL,-1,E'2021-04-14 05:40:41.564+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U225Z2P',50.09941,14.49641,E'K Žižkovu',NULL,0,E'P',-1,E'2021-04-14 05:40:41.571+02',NULL,-1,E'2021-04-14 05:40:41.571+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U267Z1P',50.12467,14.44914,E'Služská',NULL,0,E'P',-1,E'2021-04-14 05:40:41.592+02',NULL,-1,E'2021-04-14 05:40:41.592+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U330Z1P',50.1366,14.43015,E'Libeňská',NULL,0,E'P',-1,E'2021-04-14 05:40:41.603+02',NULL,-1,E'2021-04-14 05:40:41.603+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U332Z2P',50.12587,14.4896,E'Střížkov',NULL,0,E'P',-1,E'2021-04-14 05:40:41.603+02',NULL,-1,E'2021-04-14 05:40:41.603+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'C',E'U344Z3P',50.0324,14.50763,E'Litochlebské náměstí',NULL,0,E'P',-1,E'2021-04-14 05:40:41.605+02',NULL,-1,E'2021-04-14 05:40:41.605+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U345Z1P',50.13213,14.4279,E'Katovická',NULL,0,E'P',-1,E'2021-04-14 05:40:41.605+02',NULL,-1,E'2021-04-14 05:40:41.605+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'C',E'U359Z3P',50.08562,14.51224,E'Malešické náměstí',NULL,0,E'P',-1,E'2021-04-14 05:40:41.606+02',NULL,-1,E'2021-04-14 05:40:41.606+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U371Z1P',50.12769,14.4168,E'Krakov',NULL,0,E'P',-1,E'2021-04-14 05:40:41.608+02',NULL,-1,E'2021-04-14 05:40:41.608+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U409Z2P',50.04708,14.51556,E'Na Košíku',NULL,0,E'P',-1,E'2021-04-14 05:40:41.615+02',NULL,-1,E'2021-04-14 05:40:41.615+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U467Z2P',50.12922,14.48268,E'Třebenická',NULL,0,E'P',-1,E'2021-04-14 05:40:41.628+02',NULL,-1,E'2021-04-14 05:40:41.628+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'D',E'U474Z4P',50.11063,14.50247,E'Vysočanská',NULL,0,E'P',-1,E'2021-04-14 05:40:41.629+02',NULL,-1,E'2021-04-14 05:40:41.629+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U510Z2P',50.10598,14.49209,E'Českomoravská',NULL,0,E'P',-1,E'2021-04-14 05:40:41.637+02',NULL,-1,E'2021-04-14 05:40:41.638+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U511Z1P',50.13102,14.4231,E'Odra',NULL,0,E'P',-1,E'2021-04-14 05:40:41.638+02',NULL,-1,E'2021-04-14 05:40:41.638+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'E',E'U52Z5P',50.03109,14.49152,E'Chodov',NULL,0,E'P',-1,E'2021-04-14 05:40:41.521+02',NULL,-1,E'2021-04-14 05:40:41.521+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U543Z1P',50.1259,14.44278,E'Písečná',NULL,0,E'P',-1,E'2021-04-14 05:40:41.645+02',NULL,-1,E'2021-04-14 05:40:41.645+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U544Z1P',50.12757,14.43537,E'Čimický háj',NULL,0,E'P',-1,E'2021-04-14 05:40:41.645+02',NULL,-1,E'2021-04-14 05:40:41.645+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U574Z2P',50.09233,14.51215,E'Pod Táborem',NULL,0,E'P',-1,E'2021-04-14 05:40:41.648+02',NULL,-1,E'2021-04-14 05:40:41.648+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U581Z1P',50.13015,14.4309,E'Podhajská pole',NULL,0,E'P',-1,E'2021-04-14 05:40:41.648+02',NULL,-1,E'2021-04-14 05:40:41.648+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'C',E'U596Z3P',50.05383,14.51945,E'Obchodní centrum Hostivař',NULL,0,E'P',-1,E'2021-04-14 05:40:41.65+02',NULL,-1,E'2021-04-14 05:40:41.65+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'C',E'U603Z3P',50.1197,14.49765,E'Prosek',NULL,0,E'P',-1,E'2021-04-14 05:40:41.651+02',NULL,-1,E'2021-04-14 05:40:41.651+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U641Z2P',50.04617,14.52428,E'Selská',NULL,0,E'P',-1,E'2021-04-14 05:40:41.655+02',NULL,-1,E'2021-04-14 05:40:41.655+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U649Z1P',50.12746,14.41331,E'Poliklinika Mazurská',NULL,0,E'P',-1,E'2021-04-14 05:40:41.657+02',NULL,-1,E'2021-04-14 05:40:41.657+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'E',E'U660Z5P',50.08304,14.50967,E'Sídliště Malešice',NULL,0,E'P',-1,E'2021-04-14 05:40:41.658+02',NULL,-1,E'2021-04-14 05:40:41.658+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U665Z2P',50.12293,14.49342,E'Sídliště Prosek',NULL,0,E'P',-1,E'2021-04-14 05:40:41.658+02',NULL,-1,E'2021-04-14 05:40:41.658+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'D',E'U670Z4P',50.05718,14.51294,E'Sídliště Zahradní Město',NULL,0,E'P',-1,E'2021-04-14 05:40:41.659+02',NULL,-1,E'2021-04-14 05:40:41.659+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'D',E'U673Z4P',50.06455,14.50535,E'Na Padesátém',NULL,0,E'P',-1,E'2021-04-14 05:40:41.659+02',NULL,-1,E'2021-04-14 05:40:41.659+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'C',E'U675Z3P',50.12486,14.45538,E'Kobylisy',NULL,0,E'P',-1,E'2021-04-14 05:40:41.659+02',NULL,-1,E'2021-04-14 05:40:41.659+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'D',E'U694Z4P',50.09232,14.49801,E'Spojovací',NULL,0,E'P',-1,E'2021-04-14 05:40:41.664+02',NULL,-1,E'2021-04-14 05:40:41.664+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'C',E'U740Z3P',50.12774,14.47612,E'Štěpničná',NULL,0,E'P',-1,E'2021-04-14 05:40:41.669+02',NULL,-1,E'2021-04-14 05:40:41.669+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U74Z1P',50.14072,14.43083,E'Čimice',NULL,0,E'P',-1,E'2021-04-14 05:40:41.525+02',NULL,-1,E'2021-04-14 05:40:41.525+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U774Z1P',50.02048,14.5077,E'U Dálnice',NULL,0,E'P',-1,E'2021-04-14 05:40:41.673+02',NULL,-1,E'2021-04-14 05:40:41.673+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'C',E'U78Z3P',50.12663,14.47023,E'Ládví',NULL,0,E'P',-1,E'2021-04-14 05:40:41.525+02',NULL,-1,E'2021-04-14 05:40:41.525+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U806Z1P',50.13387,14.43022,E'Řepínská',NULL,0,E'P',-1,E'2021-04-14 05:40:41.679+02',NULL,-1,E'2021-04-14 05:40:41.679+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U827Z2P',50.07858,14.5082,E'Limuzská',NULL,0,E'P',-1,E'2021-04-14 05:40:41.682+02',NULL,-1,E'2021-04-14 05:40:41.682+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U889Z1P',50.06078,14.50514,E'Zahradní Město',NULL,2,E'P',-1,E'2021-04-14 05:40:41.689+02',NULL,-1,E'2021-04-14 05:40:41.689+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'D',E'U912Z4P',50.13062,14.41722,E'Zhořelecká',NULL,0,E'P',-1,E'2021-04-14 05:40:41.692+02',NULL,-1,E'2021-04-14 05:40:41.692+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U949Z1P',50.14319,14.43131,E'Sídliště Čimice',NULL,0,E'P',-1,E'2021-04-14 05:40:41.719+02',NULL,-1,E'2021-04-14 05:40:41.719+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U949Z2P',50.14492,14.4326,E'Sídliště Čimice',NULL,0,E'P',-1,E'2021-04-14 05:40:41.719+02',NULL,-1,E'2021-04-14 05:40:41.719+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'D',E'U953Z7P',50.0681,14.50808,E'Skalka',NULL,0,E'P',-1,E'2021-04-14 05:40:41.72+02',NULL,-1,E'2021-04-14 05:40:41.72+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U972Z1P',50.02251,14.48917,E'U Kunratického lesa',NULL,0,E'P',-1,E'2021-04-14 05:40:41.721+02',NULL,-1,E'2021-04-14 05:40:41.721+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'A',E'U982Z1P',50.02143,14.49371,E'Volha',NULL,0,E'P',-1,E'2021-04-14 05:40:41.722+02',NULL,-1,E'2021-04-14 05:40:41.722+02',NULL,NULL,NULL,NULL,NULL),
(0,NULL,E'B',E'U99Z2P',50.03648,14.50855,E'Donovalská',NULL,0,E'P',-1,E'2021-04-14 05:40:41.54+02',NULL,-1,E'2021-04-14 05:40:41.54+02',NULL,NULL,NULL,NULL,NULL);
-- <<<




-- >>> SKIP FEATURE

-- TRIP AT STOP
WITH init_values AS (SELECT NOW()::DATE AS now_date)
INSERT INTO "public"."vehiclepositions_trips"("cis_line_id","cis_trip_number","sequence_id","cis_line_short_name","created_at","gtfs_route_id","gtfs_route_short_name","gtfs_trip_id","id","updated_at","start_cis_stop_id","start_cis_stop_platform_code","start_time","start_timestamp","vehicle_type_id","wheelchair_accessible","create_batch_id","created_by","update_batch_id","updated_by","agency_name_scheduled","origin_route_name","agency_name_real","vehicle_registration_number","gtfs_trip_headsign","start_asw_stop_id","gtfs_route_type","gtfs_block_id","last_position_id")
VALUES
(E'100152',95,39,E'152',E'2019-05-26 10:43:16.991+02',E'L152',E'152',E'152_37_201230',E'2019-05-26T09:13:00Z_XX_152_37_201230',(SELECT CONCAT(now_date,'T09:37:10Z')::TIMESTAMP WITH TIME ZONE FROM init_values),56714,E'D',E'11:34:00',(SELECT EXTRACT(EPOCH FROM (CONCAT(now_date,'T09:34:00Z')::TIMESTAMP WITH TIME ZONE)) * 1000 FROM init_values),3,TRUE,NULL,NULL,NULL,NULL,E'ARRIVA City',E'433',E'ARRIVA City',8581,NULL,NULL,1,NULL,100015);

WITH init_values AS (SELECT NOW()::DATE AS now_date)
INSERT INTO "public"."vehiclepositions_positions"("created_at","delay","delay_stop_arrival","delay_stop_departure","next_stop_id","shape_dist_traveled","is_canceled","lat","lng","origin_time","origin_timestamp","tracking","trips_id","create_batch_id","created_by","update_batch_id","updated_at","updated_by","id","bearing","cis_last_stop_id","cis_last_stop_sequence","last_stop_id","last_stop_sequence","next_stop_sequence","speed","last_stop_arrival_time","last_stop_departure_time","next_stop_arrival_time","next_stop_departure_time","asw_last_stop_id","state_process","state_position","this_stop_id")
VALUES
(E'2021-03-31 10:53:58.936142+02',10,NULL,60,E'U337Z4',1.42,NULL,50.11063,14.50247,E'11:37:10',(SELECT EXTRACT(EPOCH FROM (CONCAT(now_date,'T09:37:10Z')::TIMESTAMP WITH TIME ZONE)) * 1000 FROM init_values),2,E'2019-05-26T09:13:00Z_XX_152_37_201230',NULL,NULL,NULL,(SELECT CONCAT(now_date,'T09:37:10Z')::TIMESTAMP WITH TIME ZONE FROM init_values),NULL,100015,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'U474Z4P');

-- TRIP CANCELED
WITH init_values AS (SELECT NOW()::DATE AS now_date)
INSERT INTO "public"."vehiclepositions_trips"("cis_line_id","cis_trip_number","sequence_id","cis_line_short_name","created_at","gtfs_route_id","gtfs_route_short_name","gtfs_trip_id","id","updated_at","start_cis_stop_id","start_cis_stop_platform_code","start_time","start_timestamp","vehicle_type_id","wheelchair_accessible","create_batch_id","created_by","update_batch_id","updated_by","agency_name_scheduled","origin_route_name","agency_name_real","vehicle_registration_number","gtfs_trip_headsign","start_asw_stop_id","gtfs_route_type","gtfs_block_id","last_position_id")
VALUES
(E'100152',95,39,E'152',E'2019-05-26 10:43:16.991+02',E'L152',E'152',E'152_60_201230',E'2019-05-26T09:13:00Z_XX_152_60_201230',(SELECT CONCAT(now_date,'T09:42:10Z')::TIMESTAMP WITH TIME ZONE FROM init_values),56714,E'D',E'11:39:00',(SELECT EXTRACT(EPOCH FROM (CONCAT(now_date,'T09:39:00Z')::TIMESTAMP WITH TIME ZONE)) * 1000 FROM init_values),3,TRUE,NULL,NULL,NULL,NULL,E'ARRIVA City',E'433',E'ARRIVA City',8581,NULL,NULL,1,NULL,100016);

WITH init_values AS (SELECT NOW()::DATE AS now_date)
INSERT INTO "public"."vehiclepositions_positions"("created_at","delay","delay_stop_arrival","delay_stop_departure","next_stop_id","shape_dist_traveled","is_canceled","lat","lng","origin_time","origin_timestamp","tracking","trips_id","create_batch_id","created_by","update_batch_id","updated_at","updated_by","id","bearing","cis_last_stop_id","cis_last_stop_sequence","last_stop_id","last_stop_sequence","next_stop_sequence","speed","last_stop_arrival_time","last_stop_departure_time","next_stop_arrival_time","next_stop_departure_time","asw_last_stop_id","state_process","state_position","this_stop_id")
VALUES
(E'2021-03-31 10:53:58.936142+02',10,NULL,60,E'U337Z4',1,'TRUE',50.11063,14.50247,E'11:42:10',(SELECT EXTRACT(EPOCH FROM (CONCAT(now_date,'T09:42:10Z')::TIMESTAMP WITH TIME ZONE)) * 1000 FROM init_values),2,E'2019-05-26T09:13:00Z_XX_152_60_201230',NULL,NULL,NULL,(SELECT CONCAT(now_date,'T09:42:10Z')::TIMESTAMP WITH TIME ZONE FROM init_values),NULL,100016,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);


-- <<<


-- ONE MORE THING

REFRESH MATERIALIZED VIEW "v_ropidgtfs_services_first14days";
REFRESH MATERIALIZED VIEW "v_ropidgtfs_trips_minmaxsequences";