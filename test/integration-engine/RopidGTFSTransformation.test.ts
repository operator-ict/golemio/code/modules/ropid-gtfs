import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { RopidGTFSTransformation } from "#ie/RopidGTFSTransformation";

chai.use(chaiAsPromised);

const readFile = (file: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        const stream = fs.createReadStream(file);
        const chunks: any[] = [];

        stream.on("error", (err) => {
            reject(err);
        });
        stream.on("data", (data) => {
            chunks.push(data);
        });
        stream.on("close", () => {
            resolve(Buffer.concat(chunks));
        });
    });
};

describe("RopidGTFSTransformation", () => {
    let transformation: RopidGTFSTransformation;
    let testSourceData: Record<string, any>;

    beforeEach(async () => {
        transformation = new RopidGTFSTransformation();
        const buffer = await readFile(__dirname + "/./data/ropidgtfs-data.json");
        testSourceData = JSON.parse(Buffer.from(buffer).toString("utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("RopidGTFS");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform element", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data).to.have.property("name");
        expect(data).to.have.property("data");
    });
});
