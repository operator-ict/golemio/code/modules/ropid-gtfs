import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine/connectors";
import { RopidGTFS } from "#sch/index";
import { RopidGTFSWorker } from "#ie/RopidGTFSWorker";

chai.use(chaiAsPromised);

describe("RopidGTFSWorker", () => {
    let worker: RopidGTFSWorker;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: any[];
    let agencyData: Record<string, any>;
    let testTransformedData: Record<string, any>;
    let testDataCis: any[];
    let testTransformedDataCis: Record<string, any>;
    let modelTruncateStub: SinonStub;
    let modelSaveStub: SinonStub;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });

        testData = [
            {
                filepath: "PID_GTFS/agency.txt",
                mtime: "2021-03-18T03:54:20.000Z",
                name: "agency",
                path: "agency.txt",
            },
            {
                filepath: "PID_GTFS/calendar.txt",
                mtime: "2021-03-18T03:54:32.000Z",
                name: "calendar",
                path: "calendar.txt",
            },
        ];

        agencyData = {
            agency_id: "99",
            agency_name: "Pražská integrovaná doprava",
            agency_url: "https://pid.cz",
            agency_timezone: "Europe/Prague",
            agency_lang: "cs",
            agency_phone: "+420234704560",
        };

        testTransformedData = { data: Buffer.from(JSON.stringify(agencyData)), name: "agency" };
        testDataCis = [];
        testTransformedDataCis = { cis_stop_groups: [1], cis_stops: [2] };

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() => Object.assign({ define: sandbox.stub() }));
        sandbox.stub(RedisConnector, "getConnection");

        worker = new RopidGTFSWorker();

        sandbox.stub(worker["dataSource"], "getLastModified").callsFake(() => Promise.resolve("2019-01-18T03:22:09.000Z"));
        sandbox.stub(worker["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));

        sandbox
            .stub(worker["metaModel"], "getLastModified")
            .callsFake(() => Object.assign({ lastModified: "2019-01-18T03:24:09.000Z", version: 0 }));
        sandbox.stub(worker["metaModel"], "save");
        sandbox.stub(worker["metaModel"], "checkSavedRows");
        sandbox.stub(worker["metaModel"], "replaceTables");
        sandbox.stub(worker["metaModel"], "rollbackFailedSaving");
        sandbox.stub(worker["metaModel"], "updateState");
        sandbox.stub(worker["metaModel"], "updateSavedRows");
        sandbox.stub(worker["metaModel"], "checkIfNewVersionIsAlreadyDeployed");
        sandbox.stub(worker["metaModel"], "checkAllTablesHasSavedState").callsFake(() => Promise.resolve(true));

        sandbox.stub(worker["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));

        sandbox.stub(worker, "sendMessageToExchange" as any);
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + RopidGTFS.name.toLowerCase();
        sandbox.stub(worker["redisModel"], "getData").callsFake(async () => Buffer.from(JSON.stringify(agencyData)));
        modelTruncateStub = sandbox.stub();
        modelSaveStub = sandbox.stub();
        sandbox.stub(worker, "getModelByName" as any).callsFake(() =>
            Object.assign({
                save: modelSaveStub,
                saveBySqlFunction: modelSaveStub,
                truncate: modelTruncateStub,
            })
        );
        sandbox.stub(worker, "saveDataToTmp" as any);

        sandbox.stub(worker["dataSourceCisStops"], "getAll").callsFake(() => Promise.resolve(testDataCis));
        sandbox
            .stub(worker["dataSourceCisStops"], "getLastModified")
            .callsFake(() => Object.assign({ lastModified: "2019-01-18T03:24:09.000Z", version: 0 }));
        sandbox.stub(worker["transformationCisStops"], "transform").callsFake(() => Promise.resolve(testTransformedDataCis));
        sandbox.stub(worker["cisStopGroupsModel"], "truncate");
        sandbox.stub(worker["cisStopGroupsModel"], "save");
        sandbox.stub(worker["cisStopsModel"], "truncate");
        sandbox.stub(worker["cisStopsModel"], "save");
        sandbox.stub(worker["delayComputationTripsModel"], "truncate");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by checkForNewData method", async () => {
        await worker.checkForNewData();
        sandbox.assert.calledOnce(worker["dataSource"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(worker["dataSourceCisStops"].getLastModified as SinonSpy);
        sandbox.assert.calledTwice(worker["metaModel"].getLastModified as SinonSpy);
        sandbox.assert.calledTwice(worker["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.callOrder(
            worker["dataSource"].getLastModified as SinonSpy,
            worker["metaModel"].getLastModified as SinonSpy,
            worker["dataSourceCisStops"].getLastModified as SinonSpy,
            worker["metaModel"].getLastModified as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by downloadFiles method", async () => {
        const promise = worker.downloadFiles();
        await sandbox.clock.tickAsync(5 * 60 * 1000);
        await promise;
        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["dataSource"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(worker["metaModel"].getLastModified as SinonSpy);
        sandbox.assert.calledThrice(worker["metaModel"].save as SinonSpy);

        sandbox.assert.callCount(worker["sendMessageToExchange"] as SinonSpy, 3);
        testData.map((f) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".transformAndSaveData",
                JSON.stringify(f)
            );
        });
        sandbox.assert.calledWith(
            worker["sendMessageToExchange"] as SinonSpy,
            "workers." + queuePrefix + ".checkSavedRowsAndReplaceTables",
            "Just do it!"
        );
        sandbox.assert.callOrder(
            worker["dataSource"].getAll as SinonSpy,
            worker["metaModel"].save as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by transformAndSaveData method", async () => {
        await worker.transformAndSaveData({ content: Buffer.from(JSON.stringify(testData[0])) });
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["transformation"].transform as SinonSpy, {
            ...testData[0],
            data: Buffer.from(JSON.stringify(agencyData)),
        });
        sandbox.assert.calledOnce(worker["getModelByName"] as SinonSpy);
        sandbox.assert.calledWith(worker["getModelByName"] as SinonSpy, testTransformedData.name);
        sandbox.assert.calledOnce(modelTruncateStub);
        sandbox.assert.calledOnce(worker["saveDataToTmp"] as SinonSpy);
        sandbox.assert.calledOnce(worker["metaModel"].getLastModified as SinonSpy);
        sandbox.assert.calledOnce(worker["metaModel"].updateState as SinonSpy);
    });

    it("should calls the correct methods by checkSavedRowsAndReplaceTables method", async () => {
        try {
            await worker.checkSavedRowsAndReplaceTables();
        } catch (err) {
            expect(err).not.to.be.null;
        }
    });

    it("should calls the correct methods by downloadCisStops method", async () => {
        await worker.downloadCisStops();
        sandbox.assert.calledOnce(worker["dataSourceCisStops"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["metaModel"].getLastModified as SinonSpy);
        sandbox.assert.callCount(worker["metaModel"].save as SinonSpy, 4);
        sandbox.assert.calledOnce(worker["transformationCisStops"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["cisStopGroupsModel"].truncate as SinonSpy);
        sandbox.assert.calledOnce(worker["cisStopGroupsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["cisStopsModel"].truncate as SinonSpy);
        sandbox.assert.calledOnce(worker["cisStopsModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["metaModel"].checkSavedRows as SinonSpy);
        sandbox.assert.calledOnce(worker["metaModel"].replaceTables as SinonSpy);
        sandbox.assert.callOrder(
            worker["dataSourceCisStops"].getAll as SinonSpy,
            worker["metaModel"].getLastModified as SinonSpy,
            worker["metaModel"].save as SinonSpy,
            worker["metaModel"].save as SinonSpy,
            worker["transformationCisStops"].transform as SinonSpy,
            worker["cisStopGroupsModel"].truncate as SinonSpy,
            worker["cisStopGroupsModel"].save as SinonSpy,
            worker["metaModel"].save as SinonSpy,
            worker["cisStopsModel"].truncate as SinonSpy,
            worker["cisStopsModel"].save as SinonSpy,
            worker["metaModel"].save as SinonSpy,
            worker["metaModel"].checkSavedRows as SinonSpy,
            worker["metaModel"].replaceTables as SinonSpy
        );
        sandbox.assert.callCount(PostgresConnector.getConnection as SinonSpy, 5);
    });
});
