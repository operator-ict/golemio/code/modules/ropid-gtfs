import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";
import Sequelize from "@golemio/core/dist/shared/sequelize";

// MSO = Mongoose SchemaObject
// SDMA = Sequelize DefineModelAttributes

const outputAgencySDMA: Sequelize.ModelAttributes<any> = {
    agency_email: Sequelize.STRING,
    agency_fare_url: Sequelize.STRING,
    agency_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    agency_lang: Sequelize.STRING,
    agency_name: Sequelize.STRING,
    agency_phone: Sequelize.STRING,
    agency_timezone: Sequelize.STRING,
    agency_url: Sequelize.STRING,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputAgencyMSO: SchemaDefinition = {
    agency_fare_url: { type: String },
    agency_id: { type: String, required: true },
    agency_lang: { type: String },
    agency_name: { type: String },
    agency_phone: { type: String },
    agency_timezone: { type: String },
    agency_url: { type: String },
};

const outputCalendarSDMA: Sequelize.ModelAttributes<any> = {
    end_date: Sequelize.STRING,
    friday: Sequelize.INTEGER,
    monday: Sequelize.INTEGER,
    saturday: Sequelize.INTEGER,
    service_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    start_date: Sequelize.STRING,
    sunday: Sequelize.INTEGER,
    thursday: Sequelize.INTEGER,
    tuesday: Sequelize.INTEGER,
    wednesday: Sequelize.INTEGER,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputCalendarMSO: SchemaDefinition = {
    end_date: { type: String },
    friday: { type: Number },
    monday: { type: Number },
    saturday: { type: Number },
    service_id: { type: String, required: true },
    start_date: { type: String },
    sunday: { type: Number },
    thursday: { type: Number },
    tuesday: { type: Number },
    wednesday: { type: Number },
};

const outputCalendarDatesSDMA: Sequelize.ModelAttributes<any> = {
    date: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    exception_type: Sequelize.INTEGER,
    service_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputCalendarDatesMSO: SchemaDefinition = {
    date: { type: String, required: true },
    exception_type: { type: Number },
    service_id: { type: String, required: true },
};

const outputRoutesSDMA: Sequelize.ModelAttributes<any> = {
    agency_id: Sequelize.STRING,
    is_night: Sequelize.STRING,
    is_regional: Sequelize.STRING,
    is_substitute_transport: Sequelize.STRING,
    route_color: Sequelize.STRING,
    route_desc: Sequelize.STRING,
    route_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    route_long_name: Sequelize.STRING,
    route_short_name: Sequelize.STRING,
    route_text_color: Sequelize.STRING,
    route_type: Sequelize.STRING,
    route_url: Sequelize.STRING,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputRoutesMSO: SchemaDefinition = {
    agency_id: { type: String },
    is_night: { type: String },
    is_regional: { type: String },
    is_substitute_transport: { type: String },
    route_color: { type: String },
    route_desc: { type: String },
    route_id: { type: String, required: true },
    route_long_name: { type: String },
    route_short_name: { type: String },
    route_text_color: { type: String },
    route_type: { type: String },
    route_url: { type: String },
};

const outputShapesSDMA: Sequelize.ModelAttributes<any> = {
    shape_dist_traveled: Sequelize.DOUBLE,
    shape_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    shape_pt_lat: Sequelize.DOUBLE,
    shape_pt_lon: Sequelize.DOUBLE,
    shape_pt_sequence: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputShapesMSO: SchemaDefinition = {
    shape_dist_traveled: { type: Number },
    shape_id: { type: String, required: true },
    shape_pt_lat: { type: Number },
    shape_pt_lon: { type: Number },
    shape_pt_sequence: { type: Number, required: true },
};

const outputStopTimesSDMA: Sequelize.ModelAttributes<any> = {
    arrival_time: Sequelize.STRING,
    arrival_time_seconds: Sequelize.INTEGER, // neni v GTFS
    departure_time: Sequelize.STRING,
    departure_time_seconds: Sequelize.INTEGER, // neni v GTFS
    drop_off_type: Sequelize.STRING,
    pickup_type: Sequelize.STRING,
    shape_dist_traveled: Sequelize.DOUBLE,
    stop_headsign: Sequelize.STRING,
    stop_id: Sequelize.STRING,
    stop_sequence: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    timepoint: Sequelize.INTEGER,
    trip_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputStopTimesMSO: SchemaDefinition = {
    arrival_time: { type: String },
    arrival_time_seconds: { type: Number },
    departure_time: { type: String },
    departure_time_seconds: { type: Number },
    drop_off_type: { type: String },
    pickup_type: { type: String },
    shape_dist_traveled: { type: Number },
    stop_headsign: { type: String },
    stop_id: { type: String },
    stop_sequence: { type: Number, required: true },
    trip_id: { type: String, required: true },
};

const outputStopsSDMA: Sequelize.ModelAttributes<any> = {
    level_id: Sequelize.STRING,
    location_type: Sequelize.INTEGER,
    parent_station: Sequelize.STRING,
    platform_code: Sequelize.STRING,
    stop_code: Sequelize.STRING,
    stop_desc: Sequelize.STRING,
    stop_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    stop_lat: Sequelize.DOUBLE,
    stop_lon: Sequelize.DOUBLE,
    stop_name: Sequelize.STRING,
    stop_timezone: Sequelize.STRING,
    stop_url: Sequelize.STRING,
    wheelchair_boarding: Sequelize.INTEGER,
    zone_id: Sequelize.STRING,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputStopsMSO: SchemaDefinition = {
    location_type: { type: Number },
    parent_station: { type: String },
    platform_code: { type: String },
    stop_id: { type: String, required: true },
    stop_lat: { type: Number },
    stop_lon: { type: Number },
    stop_name: { type: String },
    stop_url: { type: String },
    wheelchair_boarding: { type: Number },
    zone_id: { type: String },
};

const outputTripsSDMA: Sequelize.ModelAttributes<any> = {
    bikes_allowed: Sequelize.INTEGER,
    block_id: Sequelize.STRING,
    direction_id: Sequelize.INTEGER,
    exceptional: Sequelize.INTEGER, // neni v GTFS
    route_id: Sequelize.STRING,
    service_id: Sequelize.STRING,
    shape_id: Sequelize.STRING,
    trip_headsign: Sequelize.STRING,
    trip_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    trip_operation_type: Sequelize.INTEGER, // neni v GTFS
    trip_short_name: Sequelize.STRING,
    wheelchair_accessible: Sequelize.INTEGER,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputTripsMSO: SchemaDefinition = {
    bikes_allowed: { type: Number },
    block_id: { type: String },
    direction_id: { type: Number },
    exceptional: { type: Number },
    route_id: { type: String },
    service_id: { type: String },
    shape_id: { type: String },
    trip_headsign: { type: String },
    trip_id: { type: String, required: true },
    wheelchair_accessible: { type: Number },
};

const outputMetadataSDMA: Sequelize.ModelAttributes<any> = {
    dataset: Sequelize.STRING, // PID_GTFS nebo CIS_STOPS
    key: Sequelize.STRING,
    type: Sequelize.STRING, // DATASET_INFO nebo TABLE_TOTAL_COUNT
    value: Sequelize.STRING,
    version: Sequelize.INTEGER,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputMetadataMSO: SchemaDefinition = {
    dataset: { type: String }, // PID_GTFS nebo CIS_STOPS
    key: { type: String, required: true },
    type: { type: String }, // DATASET_INFO nebo TABLE_TOTAL_COUNT
    value: { type: String, required: true },
    version: { type: Number },
};

const outputCisStopGroupsSDMA: Sequelize.ModelAttributes<any> = {
    avg_jtsk_x: Sequelize.DOUBLE,
    avg_jtsk_y: Sequelize.DOUBLE,
    avg_lat: Sequelize.DOUBLE,
    avg_lon: Sequelize.DOUBLE,
    cis: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    district_code: Sequelize.STRING,
    full_name: Sequelize.STRING,
    idos_category: Sequelize.STRING,
    idos_name: Sequelize.STRING,
    municipality: Sequelize.STRING,
    name: Sequelize.STRING,
    node: Sequelize.INTEGER,
    unique_name: Sequelize.STRING,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputCisStopGroupsMSO: SchemaDefinition = {
    avg_jtsk_x: { type: Number },
    avg_jtsk_y: { type: Number },
    avg_lat: { type: Number },
    avg_lon: { type: Number },
    cis: { type: Number, required: true },
    district_code: { type: String },
    full_name: { type: String },
    idos_category: { type: String },
    idos_name: { type: String },
    municipality: { type: String },
    name: { type: String },
    node: { type: Number },
    unique_name: { type: String },
};

const outputCisStopsSDMA: Sequelize.ModelAttributes<any> = {
    alt_idos_name: Sequelize.STRING,
    cis: Sequelize.INTEGER,
    id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    jtsk_x: Sequelize.DOUBLE,
    jtsk_y: Sequelize.DOUBLE,
    lat: Sequelize.DOUBLE,
    lon: Sequelize.DOUBLE,
    platform: Sequelize.STRING,
    wheelchair_access: Sequelize.STRING,
    zone: Sequelize.STRING,

    // ⬐ Auditni pole
    create_batch_id: { type: Sequelize.BIGINT }, // ID vstupní dávky
    created_at: { type: Sequelize.DATE }, // Čas vložení
    created_by: { type: Sequelize.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: Sequelize.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: Sequelize.DATE }, // Čas poslední modifikace
    updated_by: { type: Sequelize.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

// only for Validator
const outputCisStopsMSO: SchemaDefinition = {
    alt_idos_name: { type: String },
    cis: { type: Number },
    id: { type: String, required: true },
    jtsk_x: { type: Number },
    jtsk_y: { type: Number },
    lat: { type: Number },
    lon: { type: Number },
    platform: { type: String },
    wheelchair_access: { type: String },
    zone: { type: String },
};

// only for Validator
const outputDelayComputationMSO: SchemaDefinition = {
    shape_points: [
        {
            at_stop: { type: Boolean },
            bearing: { type: Number },
            coordinates: { type: [Number] },
            distance_from_last_stop: { type: Number },
            last_stop: { type: String },
            last_stop_arrival_time_seconds: { type: Number },
            last_stop_departure_time_seconds: { type: Number },
            last_stop_sequence: { type: Number },
            next_stop: { type: String },
            next_stop_arrival_time_seconds: { type: Number },
            next_stop_departure_time_seconds: { type: Number },
            next_stop_sequence: { type: Number },
            shape_dist_traveled: { type: Number },
            this_stop: { type: String, nullable: true },
            this_stop_arrival_time_seconds: { type: Number, nullable: true },
            this_stop_departure_time_seconds: { type: Number, nullable: true },
            this_stop_sequence: { type: Number, nullable: true },
            time_scheduled_seconds: { type: Number },
        },
    ],
    trip: {
        bikes_allowed: { type: Number },
        block_id: { type: String, nullable: true },
        direction_id: { type: Number },
        exceptional: { type: Number },
        last_modify: { type: String },
        route_id: { type: String },
        service_id: { type: String },
        shape_id: { type: String },
        trip_headsign: { type: String },
        trip_id: { type: String },
        wheelchair_accessible: { type: Number },
    },
};

const outputTripsStopTimesViewSDMA: Sequelize.ModelAttributes<any> = {
    bikes_allowed: Sequelize.INTEGER,
    block_id: Sequelize.STRING,
    direction_id: Sequelize.INTEGER,
    exceptional: Sequelize.INTEGER,
    route_id: Sequelize.STRING,
    service_id: Sequelize.STRING,
    shape_id: Sequelize.STRING,
    trip_headsign: Sequelize.STRING,
    trip_id: Sequelize.STRING,
    wheelchair_accessible: Sequelize.INTEGER,

    stop_times_arrival_time: Sequelize.STRING,
    stop_times_arrival_time_seconds: Sequelize.INTEGER,
    stop_times_departure_time: Sequelize.STRING,
    stop_times_departure_time_seconds: Sequelize.INTEGER,
    stop_times_shape_dist_traveled: Sequelize.DOUBLE,
    stop_times_stop_id: Sequelize.STRING,
    stop_times_stop_sequence: Sequelize.INTEGER,
    stop_times_trip_id: Sequelize.STRING,

    stop_times_stop_stop_id: Sequelize.STRING,
    stop_times_stop_stop_lat: Sequelize.DOUBLE,
    stop_times_stop_stop_lon: Sequelize.DOUBLE,
};

const outputTripsShapesViewSDMA: Sequelize.ModelAttributes<any> = {
    bikes_allowed: Sequelize.INTEGER,
    block_id: Sequelize.STRING,
    direction_id: Sequelize.INTEGER,
    exceptional: Sequelize.INTEGER,
    route_id: Sequelize.STRING,
    service_id: Sequelize.STRING,
    shape_id: Sequelize.STRING,
    trip_headsign: Sequelize.STRING,
    trip_id: Sequelize.STRING,
    wheelchair_accessible: Sequelize.INTEGER,

    shapes_shape_dist_traveled: Sequelize.DOUBLE,
    shapes_shape_id: Sequelize.STRING,
    shapes_shape_pt_lat: Sequelize.DOUBLE,
    shapes_shape_pt_lon: Sequelize.DOUBLE,
    shapes_shape_pt_sequence: Sequelize.INTEGER,
};

const forExport = {
    name: "RopidGTFS",
    agency: {
        name: "RopidGTFSAgency",
        outputMongooseSchemaObject: outputAgencyMSO,
        outputSequelizeAttributes: outputAgencySDMA,
        pgTableName: "ropidgtfs_agency",
    },
    calendar: {
        name: "RopidGTFSCalendar",
        outputMongooseSchemaObject: outputCalendarMSO,
        outputSequelizeAttributes: outputCalendarSDMA,
        pgTableName: "ropidgtfs_calendar",
    },
    calendar_dates: {
        name: "RopidGTFSCalendarDates",
        outputMongooseSchemaObject: outputCalendarDatesMSO,
        outputSequelizeAttributes: outputCalendarDatesSDMA,
        pgTableName: "ropidgtfs_calendar_dates",
    },
    cis_stop_groups: {
        name: "RopidGTFSCisStopGroups",
        outputMongooseSchemaObject: outputCisStopGroupsMSO,
        outputSequelizeAttributes: outputCisStopGroupsSDMA,
        pgTableName: "ropidgtfs_cis_stop_groups",
    },
    cis_stops: {
        name: "RopidGTFSCisStops",
        outputMongooseSchemaObject: outputCisStopsMSO,
        outputSequelizeAttributes: outputCisStopsSDMA,
        pgTableName: "ropidgtfs_cis_stops",
    },
    delayComputationTrips: {
        mongoCollectionName: "ropidgtfs_delaycomputationtrips",
        name: "RopidGTFSDelayComputationTrips",
        outputMongooseSchemaObject: outputDelayComputationMSO,
    },
    metadata: {
        name: "RopidGTFSMetadata",
        outputMongooseSchemaObject: outputMetadataMSO,
        outputSequelizeAttributes: outputMetadataSDMA,
        pgTableName: "ropidgtfs_metadata",
    },
    routes: {
        name: "RopidGTFSRoutes",
        outputMongooseSchemaObject: outputRoutesMSO,
        outputSequelizeAttributes: outputRoutesSDMA,
        pgTableName: "ropidgtfs_routes",
    },
    shapes: {
        name: "RopidGTFSShapes",
        outputMongooseSchemaObject: outputShapesMSO,
        outputSequelizeAttributes: outputShapesSDMA,
        pgTableName: "ropidgtfs_shapes",
    },
    stop_times: {
        name: "RopidGTFSStopTimes",
        outputMongooseSchemaObject: outputStopTimesMSO,
        outputSequelizeAttributes: outputStopTimesSDMA,
        pgTableName: "ropidgtfs_stop_times",
    },
    stops: {
        name: "RopidGTFSStops",
        outputMongooseSchemaObject: outputStopsMSO,
        outputSequelizeAttributes: outputStopsSDMA,
        pgTableName: "ropidgtfs_stops",
    },
    trips: {
        name: "RopidGTFSTrips",
        outputMongooseSchemaObject: outputTripsMSO,
        outputSequelizeAttributes: outputTripsSDMA,
        pgTableName: "ropidgtfs_trips",
    },
    tripsShapesView: {
        name: "RopidGTFSTripsShapesView",
        outputSequelizeAttributes: outputTripsShapesViewSDMA,
        pgTableName: "v_ropidgtfs_trips_shapes_view",
    },
    tripsStopTimesView: {
        name: "RopidGTFSTripsStopTimesView",
        outputSequelizeAttributes: outputTripsStopTimesViewSDMA,
        pgTableName: "v_ropidgtfs_trips_stop_times_view",
    },
};

export { forExport as RopidGTFS };
