import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { Validator } from "@golemio/core/dist/shared/golemio-validator";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { DataSource, FTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { IntegrationErrorHandler, log } from "@golemio/core/dist/integration-engine/helpers";
import { PostgresModel, RedisModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import {
    DatasetEnum,
    MetaDatasetInfoKeyEnum,
    MetaStateEnum,
    MetaTypeEnum,
    RopidGTFSCisStopsTransformation,
    RopidGTFSMetadataModel,
    RopidGTFSTransformation,
} from "./";
import { RopidGTFS } from "#sch/index";
import { from as copyFrom } from "pg-copy-streams";

type RopidGTFSPgProp = keyof Pick<typeof RopidGTFS, "metadata">;

interface IDownloadedFile {
    filepath: string;
    mtime: string;
    name: string;
    path: string;
    data?: Buffer;
}

const isDownloadedFile = (body: any): body is IDownloadedFile => {
    const b = body as IDownloadedFile;
    return b.filepath !== undefined && b.mtime !== undefined && b.name !== undefined && b.path !== undefined;
};

export class RopidGTFSWorker extends BaseWorker {
    private dataSource: DataSource;
    private transformation: RopidGTFSTransformation;
    private redisModel: RedisModel;
    private metaModel: RopidGTFSMetadataModel;
    private dataSourceCisStops: DataSource;
    private transformationCisStops: RopidGTFSCisStopsTransformation;
    private cisStopGroupsModel: PostgresModel;
    private cisStopsModel: PostgresModel;
    private delayComputationTripsModel: RedisModel;
    private readonly queuePrefix: string;

    constructor() {
        super();
        this.dataSource = new DataSource(
            RopidGTFS.name + "DataSource",
            new FTPProtocolStrategy({
                filename: config.datasources.RopidGTFSFilename,
                isCompressed: true,
                path: config.datasources.RopidGTFSPath,
                tmpDir: "/tmp/",
                url: config.datasources.RopidFTP,
                whitelistedFiles: [
                    "agency.txt",
                    "calendar.txt",
                    "calendar_dates.txt",
                    "shapes.txt",
                    "stop_times.txt",
                    "stops.txt",
                    "routes.txt",
                    "trips.txt",
                ],
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            null as any
        );
        this.transformation = new RopidGTFSTransformation();
        this.redisModel = new RedisModel(
            RopidGTFS.name + "Model",
            {
                isKeyConstructedFromData: false,
                prefix: "files",
            },
            null
        );
        this.metaModel = new RopidGTFSMetadataModel();
        const cisStopsTypeStrategy = new JSONDataTypeStrategy({ resultsPath: "stopGroups" });
        cisStopsTypeStrategy.setFilter((item) => item.cis !== 0);
        this.dataSourceCisStops = new DataSource(
            RopidGTFS.name + "CisStops",
            new FTPProtocolStrategy({
                filename: config.datasources.RopidGTFSCisStopsFilename,
                path: config.datasources.RopidGTFSCisStopsPath,
                tmpDir: "/tmp/",
                url: config.datasources.RopidFTP,
            }),
            cisStopsTypeStrategy,
            null as any
        );
        this.transformationCisStops = new RopidGTFSCisStopsTransformation();
        this.cisStopGroupsModel = new PostgresModel(
            RopidGTFS.cis_stop_groups.name + "Model",
            {
                hasTmpTable: true,
                outputSequelizeAttributes: RopidGTFS.cis_stop_groups.outputSequelizeAttributes,
                pgTableName: RopidGTFS.cis_stop_groups.pgTableName,
                savingType: "insertOnly",
            },
            new Validator(RopidGTFS.cis_stop_groups.name + "ModelValidator", RopidGTFS.cis_stop_groups.outputMongooseSchemaObject)
        );
        this.cisStopsModel = new PostgresModel(
            RopidGTFS.cis_stops.name + "Model",
            {
                hasTmpTable: true,
                outputSequelizeAttributes: RopidGTFS.cis_stops.outputSequelizeAttributes,
                pgTableName: RopidGTFS.cis_stops.pgTableName,
                savingType: "insertOnly",
            },
            new Validator(RopidGTFS.cis_stops.name + "ModelValidator", RopidGTFS.cis_stops.outputMongooseSchemaObject)
        );
        this.delayComputationTripsModel = new RedisModel(
            RopidGTFS.delayComputationTrips.name + "Model",
            {
                decodeDataAfterGet: JSON.parse,
                encodeDataBeforeSave: JSON.stringify,
                isKeyConstructedFromData: true,
                prefix: RopidGTFS.delayComputationTrips.mongoCollectionName,
            },
            new Validator(
                RopidGTFS.delayComputationTrips.name + "ModelValidator",
                RopidGTFS.delayComputationTrips.outputMongooseSchemaObject
            )
        );
        this.queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + RopidGTFS.name.toLowerCase();
    }

    /**
     * Queue listener method - triggers entire flow
     */
    public checkForNewData = async (): Promise<void> => {
        // checking PID_GTFS dataset
        const serverLastModified = await this.dataSource.getLastModified();
        const dbLastModified = await this.metaModel.getLastModified(DatasetEnum.PID_GTFS);
        if (serverLastModified !== dbLastModified.lastModified) {
            await this.sendMessageToExchange("workers." + this.queuePrefix + ".downloadFiles", "Just do it!");
        }

        // checking CIS_STOPS dataset
        const CISserverLastModified = await this.dataSourceCisStops.getLastModified();
        const CISdbLastModified = await this.metaModel.getLastModified(DatasetEnum.CIS_STOPS);
        if (CISserverLastModified !== CISdbLastModified.lastModified) {
            await this.sendMessageToExchange("workers." + this.queuePrefix + ".downloadCisStops", "Just do it!");
        }
    };

    /**
     * Queue listener method - triggers processing of PID_GTFS dataset
     */
    public downloadFiles = async (): Promise<void> => {
        const files = await this.dataSource.getAll();
        const lastModified = await this.dataSource.getLastModified();
        const dbLastModified = await this.metaModel.getLastModified(DatasetEnum.PID_GTFS);
        await this.metaModel.save({
            dataset: DatasetEnum.PID_GTFS,
            key: MetaDatasetInfoKeyEnum.LAST_MODIFIED,
            type: MetaTypeEnum.DATASET_INFO,
            value: lastModified,
            version: dbLastModified.version + 1 || 1,
        });

        // send messages for transformation
        const promises = files.map(async (file: IDownloadedFile) => {
            // save meta
            await this.metaModel.save({
                dataset: DatasetEnum.PID_GTFS,
                key: file.name,
                type: MetaTypeEnum.STATE,
                value: MetaStateEnum.DOWNLOADED,
                version: dbLastModified.version + 1 || 1,
            });
            return this.sendMessageToExchange("workers." + this.queuePrefix + ".transformAndSaveData", JSON.stringify(file));
        });
        await Promise.all(promises);

        // sleep 5 minute send checkSavedRowsAndReplaceTables
        await new Promise((done) => setTimeout(done, 5 * 60 * 1000));
        await this.sendMessageToExchange("workers." + this.queuePrefix + ".checkSavedRowsAndReplaceTables", "Just do it!");
    };

    /**
     * Queue listener method - transform and save PID_GTFS dataset to tmp
     */
    public transformAndSaveData = async (msg: any): Promise<void> => {
        let inputData;
        try {
            inputData = JSON.parse(msg.content.toString());
            if (!isDownloadedFile(inputData)) {
                throw new Error("Input data has invalid format.");
            }
        } catch (err) {
            throw new CustomError("Error while parsing input data", true, this.constructor.name, undefined, err);
        }
        inputData.data = await this.redisModel.getData(inputData.filepath);
        const transformedData = await this.transformation.transform(inputData);
        const model = this.getModelByName(transformedData.name);
        await model?.truncate(true);

        await this.saveDataToTmp(transformedData);

        // update meta
        const dbLastModified = await this.metaModel.getLastModified(DatasetEnum.PID_GTFS);
        await this.metaModel.updateState(DatasetEnum.PID_GTFS, transformedData.name, MetaStateEnum.SAVED, dbLastModified.version);
    };

    /**
     * Queue listener method - verify tmp tables and replace public
     */
    public checkSavedRowsAndReplaceTables = async (): Promise<boolean> => {
        const dbLastModified = await this.metaModel.getLastModified(DatasetEnum.PID_GTFS);
        const alreadyDeployed = await this.metaModel.checkIfNewVersionIsAlreadyDeployed(
            DatasetEnum.PID_GTFS,
            dbLastModified.version
        );

        if (alreadyDeployed) {
            return true;
        }

        const allSaved = await this.metaModel.checkAllTablesHasSavedState(DatasetEnum.PID_GTFS, dbLastModified.version);

        try {
            if (!allSaved) {
                // wait to the saving process
                log.debug("checkSavedRowsAndReplaceTables - not ready yet");
                await new Promise((done) => setTimeout(done, 5 * 60 * 1000)); // sleeps for 5 minutes
                await this.sendMessageToExchange(
                    "workers." + this.queuePrefix + ".checkSavedRowsAndReplaceTables",
                    "Just do it!"
                );
                log.debug("checkSavedRowsAndReplaceTables - new message was resent");
                return true;
            }
            await this.metaModel.replaceTables(DatasetEnum.PID_GTFS, dbLastModified.version);
            await this.delayComputationTripsModel.truncate();
            await this.metaModel.refreshMaterializedViews();
            // save meta
            await this.metaModel.save({
                dataset: DatasetEnum.PID_GTFS,
                key: MetaDatasetInfoKeyEnum.DEPLOYED,
                type: MetaTypeEnum.DATASET_INFO,
                value: "true",
                version: dbLastModified.version,
            });
            return true;
        } catch (err) {
            // log failed saving process
            await this.metaModel.rollbackFailedSaving(DatasetEnum.PID_GTFS, dbLastModified.version);
            // check number of tries
            const retry = await this.metaModel.getNumberOfDownloadRetries(DatasetEnum.PID_GTFS, dbLastModified.version);

            if (retry <= 5) {
                // send new downloadFiles, log it and finish
                await this.sendMessageToExchange("workers." + this.queuePrefix + ".downloadFiles", "Just do it!");
                IntegrationErrorHandler.handle(
                    new CustomError(
                        `Error while checking RopidGTFS saved rows. Attempt number ${retry} was resent.`,
                        true,
                        this.constructor.name,
                        5004,
                        err
                    )
                );
                return true;
            } else {
                // finish with error
                throw new CustomError("Error while checking RopidGTFS saved rows.", true, this.constructor.name, 5004, err);
            }
        }
    };

    /**
     * Queue listener method - triggers processing of CIS_STOPS dataset
     */
    public downloadCisStops = async (): Promise<void> => {
        const data = await this.dataSourceCisStops.getAll();
        const lastModified = await this.dataSourceCisStops.getLastModified();
        const dbLastModified = await this.metaModel.getLastModified(DatasetEnum.CIS_STOPS);
        await this.metaModel.save({
            dataset: DatasetEnum.CIS_STOPS,
            key: MetaDatasetInfoKeyEnum.LAST_MODIFIED,
            type: MetaTypeEnum.DATASET_INFO,
            value: lastModified,
            version: dbLastModified.version + 1,
        });

        const transformedData = await this.transformationCisStops.transform(data);

        // duplicity checking
        // TODO is it ok if all duplicate items are removed?
        const uniqueMap = new Map();
        const duplicateCisStopGroups: any[] = [];
        transformedData.cis_stop_groups.forEach((item: any) => {
            if (uniqueMap.has(item.cis)) {
                duplicateCisStopGroups.push(item.cis);
                uniqueMap.delete(item.cis);
            } else {
                uniqueMap.set(item.cis, item);
            }
        });
        const uniqueCisStopGroups = [...uniqueMap.values()];

        if (duplicateCisStopGroups.length > 0) {
            log.warn(
                "CIS_STOP_GROUPS contains duplicate cis. Items with this cis was removed: " +
                    JSON.stringify(duplicateCisStopGroups)
            );
        }

        // save meta
        await this.metaModel.save([
            {
                dataset: DatasetEnum.CIS_STOPS,
                key: "cis_stop_groups",
                type: MetaTypeEnum.TABLE_TOTAL_COUNT,
                value: uniqueCisStopGroups.length,
                version: dbLastModified.version + 1,
            },
            {
                dataset: DatasetEnum.CIS_STOPS,
                key: "cis_stops",
                type: MetaTypeEnum.TABLE_TOTAL_COUNT,
                value: transformedData.cis_stops.length,
                version: dbLastModified.version + 1,
            },
        ]);
        try {
            await this.cisStopGroupsModel.truncate(true);
            await this.cisStopGroupsModel.save(uniqueCisStopGroups, true);
            await this.metaModel.save({
                dataset: DatasetEnum.CIS_STOPS,
                key: "cis_stop_groups",
                type: MetaTypeEnum.STATE,
                value: MetaStateEnum.SAVED,
                version: dbLastModified.version + 1 || 1,
            });
            await this.cisStopsModel.truncate(true);
            await this.cisStopsModel.save(transformedData.cis_stops, true);
            await this.metaModel.save({
                dataset: DatasetEnum.CIS_STOPS,
                key: "cis_stops",
                type: MetaTypeEnum.STATE,
                value: MetaStateEnum.SAVED,
                version: dbLastModified.version + 1 || 1,
            });
            await this.metaModel.checkSavedRows(DatasetEnum.CIS_STOPS, dbLastModified.version + 1);
            await this.metaModel.replaceTables(DatasetEnum.CIS_STOPS, dbLastModified.version + 1);
        } catch (err) {
            log.error(err);
            await this.metaModel.rollbackFailedSaving(DatasetEnum.CIS_STOPS, dbLastModified.version + 1);
        }
    };

    private saveDataToTmp = async (data: any) => {
        // get connection and db client
        const connection = PostgresConnector.getConnection();
        const client = (await connection.connectionManager.getConnection({ type: "write" })) as any;

        const tmpTableName = "tmp." + RopidGTFS[data.name as RopidGTFSPgProp].pgTableName;

        // copy transformed data to tmp table by stream
        await new Promise<void>((resolve, reject) => {
            const stream = client
                .query(copyFrom(`COPY ${tmpTableName} FROM STDIN DELIMITER ',' CSV HEADER;`))
                .on("error", async (err: any) => {
                    log.debug("Copy DB error.");
                    await connection.connectionManager.releaseConnection(client);
                    return reject(err);
                })
                .on("finish", async () => {
                    log.debug(`Copying finished. (${data.name})`);
                    await connection.connectionManager.releaseConnection(client);
                    return resolve();
                });
            data.data.pipe(stream);
        });
    };

    private getModelByName = (name: string): PostgresModel | null => {
        if (RopidGTFS[name as RopidGTFSPgProp].name) {
            return new PostgresModel(
                RopidGTFS[name as RopidGTFSPgProp].name + "Model",
                {
                    hasTmpTable: true,
                    outputSequelizeAttributes: RopidGTFS[name as RopidGTFSPgProp].outputSequelizeAttributes,
                    pgTableName: RopidGTFS[name as RopidGTFSPgProp].pgTableName,
                    savingType: "insertOnly",
                },
                new Validator(
                    RopidGTFS[name as RopidGTFSPgProp].name + "ModelValidator",
                    RopidGTFS[name as RopidGTFSPgProp].outputMongooseSchemaObject
                )
            );
        }
        return null;
    };
}
