import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { RopidGTFS } from "#sch/index";

export class RopidGTFSCisStopsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = RopidGTFS.name + "CisStops";
    }

    /**
     * Overrides BaseTransformation::transform
     */
    public transform = async (data: any | any[]): Promise<any | any[]> => {
        const res: Record<string, any[]> = {
            cis_stop_groups: [],
            cis_stops: [],
        };

        const promises = data.map(async (stopGroup: any) => {
            const promisesStops = stopGroup.stops.map((stop: any) => {
                return {
                    alt_idos_name: stop.altIdosName,
                    cis: stopGroup.cis,
                    id: stop.id,
                    jtsk_x: stop.jtskX,
                    jtsk_y: stop.jtskY,
                    lat: stop.lat,
                    lon: stop.lon,
                    platform: stop.platform,
                    wheelchair_access: stop.wheelchairAccess,
                    zone: stop.zone,
                };
            });
            res.cis_stops.push(...(await Promise.all(promisesStops)));
            return {
                avg_jtsk_x: stopGroup.avgJtskX,
                avg_jtsk_y: stopGroup.avgJtskY,
                avg_lat: stopGroup.avgLat,
                avg_lon: stopGroup.avgLon,
                cis: stopGroup.cis,
                district_code: stopGroup.districtCode,
                full_name: stopGroup.fullName,
                idos_category: stopGroup.idosCategory,
                idos_name: stopGroup.idosName,
                municipality: stopGroup.municipality,
                name: stopGroup.name,
                node: stopGroup.node,
                unique_name: stopGroup.uniqueName,
            };
        });
        res.cis_stop_groups = await Promise.all(promises);
        return res;
    };

    protected transformElement = async (element: any): Promise<any> => {
        // Nothing to do.
        return;
    };
}
