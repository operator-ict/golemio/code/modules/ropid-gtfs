import csv from "csv-parser";
import stringify from "csv-stringify";
import { Readable } from "stream";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { RopidGTFS } from "#sch/index";
import { Transform } from "stream";
import { RopidGTFSMetadataProp } from "#ie/RopidGTFSMetadataModel";

export class RopidGTFSTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = RopidGTFS.name;
    }

    protected transformElement = async (element: any): Promise<{ data: Readable; name: string }> => {
        const buffer = Buffer.from(element.data, "hex");
        const readable = new Readable();
        readable._read = () => {
            // _read is required but you can noop it
        };
        readable.push(buffer);
        readable.push(null);

        // create object by table structure and fill its values with null
        const tableToObjectFilledWithNulls = Object.keys(
            RopidGTFS[element.name as RopidGTFSMetadataProp].outputSequelizeAttributes
        ).reduce((acc: any, curr) => ((acc[curr] = null), acc), {});

        const addColumns = new Transform({
            objectMode: true,
            transform(chunk, encoding, callback) {
                this.push({
                    // prepare object to be same as db table (number and order of columns)
                    ...tableToObjectFilledWithNulls,
                    // filter redundant properties and fill data to prepared structure
                    ...Object.keys(chunk)
                        .filter((key) => Object.keys(tableToObjectFilledWithNulls).includes(key))
                        .reduce((obj: any, key) => {
                            obj[key] = chunk[key];
                            return obj;
                        }, {}),
                    // fill audit fields
                    create_batch_id: -1,
                    created_at: new Date().toISOString(),
                    update_batch_id: -1,
                    updated_at: new Date().toISOString(),
                });
                callback();
            },
        });

        return {
            data: readable
                .pipe(csv())
                .pipe(addColumns)
                .pipe(stringify({ header: true })),
            name: element.name,
        };
    };
}
