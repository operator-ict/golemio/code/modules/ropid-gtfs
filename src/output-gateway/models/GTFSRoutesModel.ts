import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { RopidGTFS } from "#sch/index";

export class GTFSRoutesModel extends SequelizeModel {
    public constructor() {
        super(RopidGTFS.routes.name, RopidGTFS.routes.pgTableName, RopidGTFS.routes.outputSequelizeAttributes);
    }

    /** Retrieves all gtfs routes
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @returns Array of the retrieved records
     */
    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
        } = {}
    ): Promise<any> => {
        const { limit, offset } = options;
        try {
            const order: any = [];
            order.push([["route_id", "asc"]]);
            const data = await this.sequelizeModel.findAll({
                limit,
                offset,
                order,
            });
            return data.map((item) => this.ConvertItem(item));
        } catch (err) {
            throw new CustomError("Database error", true, "GTFSRoutesModel", 500, err);
        }
    };

    /** Retrieves specific gtfs routes
     * @param {string} id Id of the route
     * @returns Object of the retrieved record or null
     */
    public GetOne = async (id: string): Promise<any> => this.ConvertItem(await this.sequelizeModel.findByPk(id));

    /**
     * Convert a single db result to proper output format with all tranformation
     * @param {object} route Route object
     * @returns A converted item of the result
     */
    private ConvertItem = (route: any) => {
        if (route === null) return null;

        // fallback for not plain Sequelize object
        if (route.toJSON) route = route.toJSON();

        // convert to booleans
        route.is_night = route.is_night === "1";
        route.is_regional = route.is_regional === "1";
        route.is_substitute_transport = route.is_substitute_transport === "1";

        return route;
    };
}
